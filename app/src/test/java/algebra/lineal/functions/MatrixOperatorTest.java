package algebra.lineal.functions;


import algebra.lineal.classes.Matrix;
import org.junit.jupiter.api.Test;

/**
 * MatrixOperatorTest class tests MatrixOperator class.
 */
class MatrixOperatorTest {

  /**
   * productTest method tests product functions and some cases.
   */
  @Test
  public void productTest() {
    Matrix matrixA = new Matrix(3, 2, "A");
    Matrix matrixB = new Matrix(2, 2, "B");
    Matrix matrixC = new Matrix(4, 2, "C");


    System.out.println(matrixA);
    System.out.println(matrixB);
    System.out.println(matrixC);
    Matrix matrixAxB = new MatrixOperator(matrixA, matrixB, "AxB").productOf();
    System.out.println(matrixAxB);

    Matrix matrixBxA = new MatrixOperator(matrixB, matrixA, "BxA").productOf();
    System.out.println(matrixBxA);

    Matrix matrixCxA = new MatrixOperator(matrixC, matrixA, "CxA").productOf();
    System.out.println(matrixCxA);
  }

  /**
   * additionTest method tests addition functions and some cases.
   */
  @Test
  public void additionTest() {
    Matrix matrixA = new Matrix(1, 3, "A");
    Matrix matrixB = new Matrix(1, 3, "B");
    Matrix matrixC = new Matrix(2, 2, "C");


    System.out.println(matrixA);
    System.out.println(matrixB);
    System.out.println(matrixC);
    Matrix matrixAxB = new MatrixOperator(matrixA, matrixB, "A+B").additionOf();
    System.out.println(matrixAxB);

    Matrix matrixBxA = new MatrixOperator(matrixB, matrixA, "B+A").additionOf();
    System.out.println(matrixBxA);

    Matrix matrixCxA = new MatrixOperator(matrixC, matrixA, "C+A").additionOf();
    System.out.println(matrixCxA);
  }

}
