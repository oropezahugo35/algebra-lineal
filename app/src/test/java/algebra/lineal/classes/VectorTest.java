package algebra.lineal.classes;


import org.junit.jupiter.api.Test;

class VectorTest {

  @Test
  public void vectorTest() {
    Vector vectorw = new Vector(new double[]{1, 2}, "w");
    Vector vectorv = new Vector(new double[]{3, -1, 0}, "v");
    Vector vectorr = new Vector(new double[]{3}, "r");
    Vector vectors = new Vector(new double[]{3, 5, -1, 10}, "s");
    Vector vectort = new Vector(new double[]{1, 2}, new double[]{4, 4}, "t");
    Vector vectorm = new Vector(new double[]{0, 0}, "m");
    Vector vectorn = new Vector(new double[]{0, 0, 0}, "n");
    Vector vectorq = new Vector(new double[]{1, 2, 0}, new double[]{4, 4}, "q");
    Vector vectorp = new Vector(new double[]{7, 3, 1, -5}, "p");

    System.out.println(vectorw);
    System.out.println(vectorv);
    System.out.println(vectorr);
    System.out.println(vectors);
    System.out.println(vectort);
    System.out.println(vectorm);
    System.out.println(vectorn);
    System.out.println(vectorq);
    System.out.println(vectorp);

  }
}