package algebra.lineal.classes;

import org.junit.jupiter.api.Test;

class MatrixTest {

  @Test
  public void matrixTest() {
    double[][] matrixContent = new double[2][5];
    double num = 0;
    for (int i = 0; i < matrixContent.length; i++) {
      for(int j = 0; j < matrixContent[0].length; j++) {
        matrixContent[i][j] = num;
        num += 1;
      }
    }
    Matrix matrix = new Matrix(matrixContent, "A");
    System.out.println(matrix);

    Matrix matrix2 = new Matrix(9, 3, "B");
    System.out.println(matrix2);
  }

}
