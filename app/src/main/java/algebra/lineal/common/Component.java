package algebra.lineal.common;

import javafx.scene.Node;

public interface Component {
  /**
   * build method calls the configureComponent method and return his Node.
   */
  default Node build() {
    configureComponent();
    return getComponent();
  }

  /**
   * configureComponent method set custom configuration for the components when use it with Override.
   */
  void configureComponent();

  /**
   * getComponent method return the Node of the component when it is used with Override.
   */
  Node getComponent();
}
