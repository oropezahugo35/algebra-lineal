package algebra.lineal.common;

import javafx.scene.Node;

public interface ComposableComponent extends Component {

  @Override
  default Node build() {
    compose();
    return Component.super.build();
  }

  /**
   * compose method builds the "body" of a custom component.
   */
  void compose();
}
