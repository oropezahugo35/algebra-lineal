package algebra.lineal.functions;

import javax.swing.*;

public class Base {
  int quantityVectors;
  int vectorialSpace;
  boolean checker = true;
  double result;
  double[][] vectors;
  String finalScore;

  public Base() {
  }

  public Base(int quantityVectors, int vectorialSpace) {
    this.quantityVectors = quantityVectors;
    vectors = new double[quantityVectors][vectorialSpace+1]; // "<En que espacio vectorial estaran sus vectores>" 1.- R2" 2.- R3" 3.- R4" 4.- R5" 5.- R6",
    if (quantityVectors  != vectorialSpace) {
      checker = false;
    }
  }

  public int getQuantityVectors() {
    return quantityVectors;
  }

  public int getVectorialSpace() {
    return vectorialSpace;
  }

  public boolean verificador() {
    return checker;
  }

  public String isBase2(double u1, double u2, double v1, double v2) {
    if(checker) {
      if(resolution(u1, u2, v1, v2)) {// to check if it is LI
        if(resolution(u1, v1, u2, v2)) { // to see if S generates V
          return "S are the given vectors, therefore S is base of V";
        }
      }
    }
    return "S are the given vectors, therefore S isn't a basis of V";
  }

  public String isBase3(double u1, double u2, double u3, double v1, double v2, double v3, double w1, double w2, double w3) {
    if(checker) {
      if(resolution(u1, u2, u3, v1, v2, v3, w1, w2, w3)) {
        System.out.println("The determiner is " + result);
        if(resolution(u1, v1, w1, u2, v2, w2, u3, v3, w3)) { // para ver si S genera a V
          return "S are the given vectors, therefore S is base of V";
        }
      }
    }
    return "S are the given vectors, therefore S isn't a basis of V";
  }

  public String isBase4(double u1, double u2, double u3, double u4, double v1, double v2, double v3, double v4, double w1, double w2, double w3, double w4, double x1, double x2, double x3, double x4) {
    if(checker) {
      if(resolution(u1, u2, u3, u4, v1, v2, v3, v4, w1, w2, w3, w4, x1, x2, x3, x4)) {
        if(resolution(u1, v1, w1, x1, u2, v2, w2, x2, u3, v3, w3, x3, u4, v4, w4, x4)) { // para ver si S genera a V
          return "S are the given vectors, therefore S is base of V";
        }
      }
    }
    return "S are the given vectors, therefore S isn't a basis of V";
  }

  public String isBase5(double u1, double u2, double u3, double u4, double u5, double v1, double v2, double v3, double v4, double v5, double w1, double w2, double w3, double w4, double w5, double x1, double x2, double x3, double x4, double x5, double y1, double y2, double y3, double y4, double y5) {
    if(checker) {
      if(resolution(u1, u2, u3, u4 , u5, v1, v2, v3, v4, v5, w1, w2, w3, w4, w5, x1, x2, x3, x4, x5, y1, y2, y3, y4, y5)) {
        if(resolution(u1, v1, w1, x1, y1, u2, v2, w2, x2, y2, u3, v3, w3, x3, y3, u4, v4, w4, x4, y4, u5, v5, w5, x5, y5)) { // para ver si S genera a V
          return "S are the given vectors, therefore S is base of V";
        }
      }
    }
    return "S are the given vectors, therefore S isn't a basis of V";
  }

  public String isBase6(double u1, double u2, double u3, double u4, double u5, double u6, double v1, double v2, double v3, double v4, double v5, double v6, double w1, double w2, double w3, double w4, double w5, double w6, double x1, double x2, double x3, double x4, double x5, double x6, double y1, double y2, double y3, double y4, double y5, double y6, double z1, double z2, double z3, double z4, double z5, double z6) {
    if(checker) {
      if(resolution(u1, u2, u3, u4, u5, u6, v1, v2, v3, v4, v5, v6, w1, w2, w3, w4, w5, w6, x1, x2, x3, x4, x5, x6, y1, y2, y3, y4, y5, y6, z1, z2, z3, z4, z5, z6)) {
        if(resolution(u1, v1, w1, x1, y1, z1, u2, v2, w2, x2, y2, z2, u3, v3, w3, x3, y3, z3, u4, v4, w4, x4, y4, z4, u5, v5, w5, x5, y5, z5, u6, v6, w6, x6, y6, z6)) { // para ver si S genera a V
          return "S are the given vectors, therefore S is base of V";
        }
      }
    }
    return "S are the given vectors, therefore S isn't a basis of V";
  }

  public boolean resolution(double u1, double u2, double v1, double v2) {
    while(checker) {
      filledDataR2(u1, u2, v1, v2);
      result = calculateVolume();
      finalScore = "<THE RESULT IS --> " + result + " u2>";
      return verifierInverse(result);
    }
    return false;
  }

  public boolean resolution(double u1, double u2, double u3, double v1, double v2, double v3, double w1, double w2, double w3) {
    while(checker) {
      filledDataR3(u1, u2, u3, v1, v2, v3, w1, w2, w3);
      result = calculateVolume();
      finalScore = "<THE RESULT IS --> " + result + " u3>";
      return verifierInverse(result);
    }
    return false;
  }

  public boolean resolution(double u1, double u2, double u3, double u4, double v1, double v2, double v3, double v4, double w1, double w2, double w3, double w4, double x1, double x2, double x3, double x4) {
    while(checker) {
      filledDataR4(u1, u2, u3, u4, v1, v2, v3, v4, w1, w2, w3, w4, x1, x2, x3, x4);
      result = calculateVolume();
      finalScore = "<THE RESULT IS --> " + result + " u4>";
      return verifierInverse(result);
    }
    return false;
  }

  public boolean resolution(double u1, double u2, double u3, double u4, double u5, double v1, double v2, double v3, double v4, double v5, double w1, double w2, double w3, double w4, double w5, double x1, double x2, double x3, double x4, double x5, double y1, double y2, double y3, double y4, double y5) {
    while(checker) {
      filledDataR5(u1, u2, u3, u4 , u5, v1, v2, v3, v4, v5, w1, w2, w3, w4, w5, x1, x2, x3, x4, x5, y1, y2, y3, y4, y5);
      result = calculateVolume();
      finalScore = "<THE RESULT IS --> " + result + " u5>";
      return verifierInverse(result);
    }
    return false;
  }

  public boolean resolution(double u1, double u2, double u3, double u4, double u5, double u6, double v1, double v2, double v3, double v4, double v5, double v6, double w1, double w2, double w3, double w4, double w5, double w6, double x1, double x2, double x3, double x4, double x5, double x6, double y1, double y2, double y3, double y4, double y5, double y6, double z1, double z2, double z3, double z4, double z5, double z6) {
    while(checker) {
      filledDataR6(u1, u2, u3, u4, u5, u6, v1, v2, v3, v4, v5, v6, w1, w2, w3, w4, w5, w6, x1, x2, x3, x4, x5, x6, y1, y2, y3, y4, y5, y6, z1, z2, z3, z4, z5, z6);
      result = calculateVolume();
      finalScore = "<THE RESULT IS --> " + result + " u6>";
      return verifierInverse(result);
    }
    return false;
  }

  public void filledDataR2(double u1, double u2, double v1, double v2) {
    double[] values = {u1, u2, v1, v2};
    int index = 0;
    for (int i = 0; i < quantityVectors; i++) {
      for (int j = 0; j < quantityVectors; j++) {
        vectors[i][j] = values[index];
        index++;
      }
    }
  }

  public void filledDataR3(double u1, double u2, double u3, double v1, double v2, double v3, double w1, double w2, double w3) {
    double[] values = {u1, u2, u3, v1, v2, v3, w1, w2, w3};
    int index = 0;
    for (int i = 0; i < quantityVectors; i++) {
      for (int j = 0; j < quantityVectors; j++) {
        vectors[i][j] = values[index];
        index++;
      }
    }
  }

  public void filledDataR4(double u1, double u2, double u3, double u4, double v1, double v2, double v3, double v4, double w1, double w2, double w3, double w4, double x1, double x2, double x3, double x4) {
    double[] values = {u1, u2, u3, u4, v1, v2, v3, v4, w1, w2, w3, w4, x1, x2, x3, x4};
    int index = 0;
    for (int i = 0; i < quantityVectors; i++) {
      for (int j = 0; j < quantityVectors; j++) {
        vectors[i][j] = values[index];
        index++;
      }
    }
  }

  public void filledDataR5(double u1, double u2, double u3, double u4, double u5, double v1, double v2, double v3, double v4, double v5, double w1, double w2, double w3, double w4, double w5, double x1, double x2, double x3, double x4, double x5, double y1, double y2, double y3, double y4, double y5) {
    double[] values = {u1, u2, u3, u4, u5, v1, v2, v3, v4, v5, w1, w2, w3, w4, w5, x1, x2, x3, x4, x5, y1, y2, y3, y4, y5};
    int index = 0;
    for (int i = 0; i < quantityVectors; i++) {
      for (int j = 0; j < quantityVectors; j++) {
        vectors[i][j] = values[index];
        index++;
      }
    }
  }

  public void filledDataR6(double u1, double u2, double u3, double u4, double u5, double u6, double v1, double v2, double v3, double v4, double v5, double v6, double w1, double w2, double w3, double w4, double w5, double w6, double x1, double x2, double x3, double x4, double x5, double x6, double y1, double y2, double y3, double y4, double y5, double y6, double z1, double z2, double z3, double z4, double z5, double z6) {
    double[] values = {u1, u2, u3, u4, u5, u6, v1, v2, v3, v4, v5, v6, w1, w2, w3, w4, w5, w6, x1, x2, x3, x4, x5, x6, y1, y2, y3, y4, y5, y6, z1, z2, z3, z4, z5, z6};
    int index = 0;
    for (int i = 0; i < quantityVectors; i++) {
      for (int j = 0; j < quantityVectors; j++) {
        vectors[i][j] = values[index];
        index++;
      }
    }
  }

  public boolean verifierInverse(double product) {
    boolean checker = true;
    if (product == 0) {
      checker = false;
    }
    return checker;
  }

  public double calculateVolume() {
    double[] product = new double[quantityVectors];
    double result = 0.0;
    switch (quantityVectors) {
      case 2: product[0] = (vectors[0][0] * vectors[1][1]) - (vectors[0][1] * vectors[1][0]);
        result = product[0];
        break;
      case 3:
        product[0] = (vectors[1][0] * ((vectors[0][1] * vectors[2][2]) - (vectors[0][2] * vectors[2][1]))) * -1;
        product[1] = vectors[1][1] * ((vectors[0][0] * vectors[2][2]) - (vectors[0][2] * vectors[2][0]));
        product[2] = (vectors[1][2] * ((vectors[0][0] * vectors[2][1]) - (vectors[0][1] * vectors[2][0]))) * -1;
        result = product[0] + product[1] + product[2];
        break;
      case 4:
        double u[] = {vectors[0][1], vectors[0][2], vectors[0][3]};
        double v[] = {vectors[2][1], vectors[2][2], vectors[2][3]};
        double w[] = {vectors[3][1], vectors[3][2], vectors[3][3]};
        product[0] = vectors[1][0] * calculateDeterminantR3(u, v , w);
        u[0] = vectors[0][0];
        v[0] = vectors[2][0];
        w[0] = vectors[3][0];
        product[1] = (vectors[1][1] * calculateDeterminantR3(u, v , w)) * -1;
        u[1] = vectors[0][1];
        v[1] = vectors[2][1];
        w[1] = vectors[3][1];
        product[2] = vectors[1][2] * calculateDeterminantR3(u, v , w);
        u[1] = vectors[0][2];
        v[1] = vectors[2][2];
        w[1] = vectors[3][2];
        product[3] = (vectors[1][3] * calculateDeterminantR3(u, v , w)) * -1;
        result = product[0] + product[1] + product[2] + product[3];
        System.out.println(result);
        break;
      case 5:
        double a[] = {vectors[0][1], vectors[0][2], vectors[0][3], vectors[0][4]};
        double b[] = {vectors[2][1], vectors[2][2], vectors[2][3], vectors[2][4]};
        double c[] = {vectors[3][1], vectors[3][2], vectors[3][3], vectors[3][4]};
        double d[] = {vectors[4][1], vectors[4][2], vectors[4][3], vectors[4][4]};
        product[0] = vectors[1][0] * calculateDeterminantR4(a, b, c, d);
        a[0] = vectors[0][0];
        b[0] = vectors[2][0];
        c[0] = vectors[3][0];
        d[0] = vectors[4][0];
        product[1] = (vectors[1][1] * calculateDeterminantR4(a, b, c, d)) * -1;
        a[1] = vectors[0][1];
        b[1] = vectors[2][1];
        c[1] = vectors[3][1];
        d[1] = vectors[4][1];
        product[2] = vectors[1][2] * calculateDeterminantR4(a, b, c, d);
        a[2] = vectors[0][2];
        b[2] = vectors[2][2];
        c[2] = vectors[3][2];
        d[2] = vectors[4][2];
        product[3] = (vectors[1][3] * calculateDeterminantR4(a, b, c, d)) * -1;
        a[3] = vectors[0][3];
        b[3] = vectors[2][3];
        c[3] = vectors[3][3];
        d[3] = vectors[4][3];
        product[4] = vectors[1][4] * calculateDeterminantR4(a, b, c, d);
        result = product[0] + product[1] + product[2] + product[3] + product[4];
        System.out.println(result);
        break;
      case 6:
        double e[] = {vectors[0][1], vectors[0][2], vectors[0][3], vectors[0][4], vectors[0][5]};
        double f[] = {vectors[2][1], vectors[2][2], vectors[2][3], vectors[2][4], vectors[2][5]};
        double g[] = {vectors[3][1], vectors[3][2], vectors[3][3], vectors[3][4], vectors[3][5]};
        double h[] = {vectors[4][1], vectors[4][2], vectors[4][3], vectors[4][4], vectors[4][5]};
        double i[] = {vectors[5][1], vectors[5][2], vectors[5][3], vectors[5][4], vectors[5][5]};
        product[0] = (vectors[1][0] * calculateDeterminantR5(e, f, g, h ,i)) * -1;
        e[0] = vectors[0][0];
        f[0] = vectors[2][0];
        g[0] = vectors[3][0];
        h[0] = vectors[4][0];
        i[0] = vectors[5][0];
        product[1] = vectors[1][1] * calculateDeterminantR5(e, f, g, h ,i);
        e[1] = vectors[0][1];
        f[1] = vectors[2][1];
        g[1] = vectors[3][1];
        h[1] = vectors[4][1];
        i[1] = vectors[5][1];
        product[2] = (vectors[1][2] * calculateDeterminantR5(e, f, g, h ,i)) * -1;
        e[2] = vectors[0][2];
        f[2] = vectors[2][2];
        g[2] = vectors[3][2];
        h[2] = vectors[4][2];
        i[2] = vectors[5][2];
        product[3] = vectors[1][3] * calculateDeterminantR5(e, f, g, h ,i);
        e[3] = vectors[0][3];
        f[3] = vectors[2][3];
        g[3] = vectors[3][3];
        h[3] = vectors[4][3];
        i[3] = vectors[5][3];
        product[4] = (vectors[1][4] * calculateDeterminantR5(e, f, g, h ,i)) * -1;
        e[4] = vectors[0][4];
        f[4] = vectors[2][4];
        g[4] = vectors[3][4];
        h[4] = vectors[4][4];
        i[4] = vectors[5][4];
        product[5] = vectors[1][5] * calculateDeterminantR5(e, f, g, h ,i);
        result = product[0] + product[1] + product[2] + product[3] + product[4] + product[5];
        System.out.println(result);
        break;
    }
    return result;
  }

  public double calculateDeterminantR3(double[] u, double[] v, double[] w) {
    double vectorProducto[] = new double[3];
    vectorProducto[0] = ((u[0] * v[1] * w[2]) + (v[0] * w[1] * u[2]) + (w[0] * u[1] * v[2]));
    vectorProducto[1] = ((u[2] * v[1] * w[0]) + (v[2] * w[1] * u[0]) + (w[2] * u[1] * v[0])) * -1;
    double producto = vectorProducto[0] + vectorProducto[1];
    double redondeadoProducto =  Math.round(producto*100.0)/100.0;
    return redondeadoProducto;
  }

  public double calculateDeterminantR4(double[] u, double[] v, double[] w, double[] x) {
    double vectorProducto[] = new double[4];
    double a[] = {u[1], u[2], u[3]};
    double b[] = {w[1], w[2], w[3]};
    double c[] = {x[1], x[2], x[3]};
    vectorProducto[0] = v[0] * calculateDeterminantR3(a, b, c);
    a[0] = u[0];
    b[0] = w[0];
    c[0] = x[0];
    vectorProducto[1] = (v[1] * calculateDeterminantR3(a, b, c)) * -1;
    a[1] = u[1];
    b[1] = w[1];
    c[1] = x[1];
    vectorProducto[2] = v[2] * calculateDeterminantR3(a, b, c);
    a[2] = u[2];
    b[2] = w[2];
    c[2] = x[2];
    vectorProducto[3] = (v[3] * calculateDeterminantR3(a, b, c)) * -1;
    double producto = vectorProducto[0] + vectorProducto[1] + vectorProducto[2] + vectorProducto[3];
    double redondeadoProducto =  Math.round(producto*100.0)/100.0;
    return redondeadoProducto;
  }

  public double calculateDeterminantR5(double[] u, double[] v, double[] w, double[] x, double[] y) {
    double vectorProducto[] = new double[5];
    double a[] = {u[1], u[2], u[3], u[4]};
    double b[] = {w[1], w[2], w[3], w[4]};
    double c[] = {x[1], x[2], x[3], x[4]};
    double d[] = {y[1], y[2], y[3], y[4]};
    vectorProducto[0] = v[0] * calculateDeterminantR4(a, b, c, d);
    a[0] = u[0];
    b[0] = w[0];
    c[0] = x[0];
    d[0] = x[0];
    vectorProducto[1] = (v[1] * calculateDeterminantR4(a, b, c, d)) * -1;
    a[1] = u[1];
    b[1] = w[1];
    c[1] = x[1];
    d[1] = x[1];
    vectorProducto[2] = v[2] * calculateDeterminantR4(a, b, c, d);
    a[2] = u[2];
    b[2] = w[2];
    c[2] = x[2];
    d[2] = x[2];
    vectorProducto[3] = (v[3] * calculateDeterminantR4(a, b, c, d)) * -1;
    a[3] = u[3];
    b[3] = w[3];
    c[3] = x[3];
    d[3] = x[3];
    vectorProducto[4] = v[4] * calculateDeterminantR4(a, b, c, d);
    double producto = vectorProducto[0] + vectorProducto[1] + vectorProducto[2] + vectorProducto[3] + vectorProducto[4];
    double redondeadoProducto =  Math.round(producto*100.0)/100.0;
    return redondeadoProducto;
  }
}