package algebra.lineal.functions;

import java.util.ArrayList;

public class StringManager {

  private String rowContent;
  private final int dimension;

  public StringManager(String rowContent, int dimension) {
    this.rowContent = rowContent;
    this.dimension = dimension;
  }
  public StringManager(String rowContent) {
    this.rowContent = rowContent;
    this.dimension = 5;
  }

  public double[] convert() {
    String[] list = rowContent.split("");
    String str = "";
    for(String letter: list) {
      str += (letter != " ") ? letter : "";
    }
    rowContent = str;
    String[] list2 = rowContent.split(",");
    double[] doubles = new double[dimension];
    for(int i = 0; i < this.dimension; i++) {
      doubles[i] = 0;
    }
    if(doubles.length <= list2.length) {
      for(int i = 0; i < this.dimension; i++) {
        doubles[i] = Double.parseDouble(list2[i]);
      }
    } else {
      for (int i = 0; i < list2.length; i++) {
        doubles[i] = Double.parseDouble(list2[i]);
      }
    }
    return doubles;
  }

  public double[] convertToVector() {
    String[] list = rowContent.split("");
    String str = "";
    for(String letter: list) {
      str += (letter != " ") ? letter : "";
    }
    rowContent = str;
    String[] list2 = rowContent.split(",");
    ArrayList<Double> doubles = new ArrayList<>();

    for(int i = 0; i < list2.length; i++) {
      doubles.add(Double.parseDouble(list2[i]));
    }
    double[] doubles1 = new double[doubles.size()];
    for(int i = 0; i < doubles.size(); i++) {
      doubles1[i] = doubles.get(i);
    }
    return doubles1;
  }


}
