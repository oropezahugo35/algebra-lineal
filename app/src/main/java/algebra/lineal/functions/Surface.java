package algebra.lineal.functions;

import javax.swing.*;

public class Surface {
  double u[] = new double[3];

  double v[] = new double[3];
  double result;
  public Surface() {
  }
  public String menuCalculationSurfaces() {
    int option;
    do {
      option = Integer.parseInt(JOptionPane.showInputDialog(null, "<Is the area to be calculated a?>" + "\n1.- Parallelogram" + "\n2.- Triangle" + "\n0.- Leave", "Requesting data", JOptionPane.ERROR_MESSAGE));
      switch (option) {
        case 1: filledDataR3();
          result = calculateSurface(u, v);
          JOptionPane.showMessageDialog(null, "<THE RESULT IS --> " + result + ">");
          break;
        case 2: filledDataR3();
          result = calculateSurface(u, v);
          JOptionPane.showMessageDialog(null, "<THE RESULT IS --> " + (result /2) + ">");
          break;
        default: if (option != 0) {
          JOptionPane.showMessageDialog(null, "<ENTER VALID OPTION>");
        }
          break;
      }
    } while(option != 0);
    return "ENDED PROCESS";
  }

  public String menuCalculationSurfaces(int number, double u1, double u2, double u3, double v1, double v2, double v3) {
    String finalScore = "";
    switch (number) {
      case 1: filledDataR3(u1, u2, u3, v1, v2, v3);
        result = calculateSurface(u, v);
        finalScore += "<THE RESULT IS --> " + result + ">";
        break;
      case 2: filledDataR3(u1, u2, u3, v1, v2, v3);
        result = calculateSurface(u, v);
        finalScore +=  "<THE RESULT IS --> " + (result /2) + ">";
        break;
    }
    return finalScore;
  }

  public void filledDataR3() {
    JOptionPane.showMessageDialog(null, "<First Vector Data Fill>");
    u[0] = Double.parseDouble(JOptionPane.showInputDialog(null, "<Enter the number of the element that is in the position 'x'>", "Requesting data", JOptionPane.ERROR_MESSAGE));
    u[1] = Double.parseDouble(JOptionPane.showInputDialog(null, "<Enter the number of the element that is in the position 'y'>", "Requesting data", JOptionPane.ERROR_MESSAGE));
    u[2] = Double.parseDouble(JOptionPane.showInputDialog(null, "<Enter the number of the element that is in the position 'z'>", "Requesting data", JOptionPane.ERROR_MESSAGE));
    JOptionPane.showMessageDialog(null, "<Second Vector Data Fill>");
    v[0] = Double.parseDouble(JOptionPane.showInputDialog(null, "<Enter the number of the element that is in the position 'x'>", "Requesting data", JOptionPane.ERROR_MESSAGE));
    v[1] = Double.parseDouble(JOptionPane.showInputDialog(null, "<Enter the number of the element that is in the position 'y'>", "Requesting data", JOptionPane.ERROR_MESSAGE));
    v[2] = Double.parseDouble(JOptionPane.showInputDialog(null, "<Enter the number of the element that is in the position 'z'>", "Requesting data", JOptionPane.ERROR_MESSAGE));
  }

  public void filledDataR3(double u1, double u2, double u3, double v1, double v2, double v3) {
    u[0] = u1;
    u[1] = u2;
    u[2] = u3;
    v[0] = v1;
    v[1] = v2;
    v[2] = v3;
  }

  public double calculateSurface(double[] u, double[] v) {
    double vectorProduct[] = new double[3];
    vectorProduct[0] = (u[1] * v[2]) - (u[2] * v[1]);
    vectorProduct[1] = ((u[0] * v[2]) - (u[2] * v[0])) * -1;
    vectorProduct[2] = (u[0] * v[1]) - (u[1] * v[0]);
    double product =Math.sqrt(Math.pow(vectorProduct[0], 2) + Math.pow(vectorProduct[1], 2) + Math.pow(vectorProduct[2], 2));
    double roundedProduct =  Math.round(product*100.0)/100.0;
    return roundedProduct;
  }
}
