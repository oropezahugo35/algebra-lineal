package algebra.lineal.functions;

import algebra.lineal.classes.Matrix;

/**
 * MatrixVerifiers class contents verifiers for matrix operations.
 */
public class MatrixVerifiers {

  /**
   * verifyProduct method verifies if a matrix multiplication is possible.
   */
  protected static boolean verifyProduct(Matrix matrix1, Matrix matrix2) {
    return (matrix1.getDimension2() == matrix2.getDimension1());
  }

  /**
   * verifyAddition method verifies if a matrix addition is possible.
   */
  protected static boolean verifyAddition(Matrix matrix1, Matrix matrix2) {
    return (matrix1.getDimension2() == matrix2.getDimension2() && matrix1.getDimension1() == matrix2.getDimension1());
  }
}
