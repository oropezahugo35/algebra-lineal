package algebra.lineal.functions;

import javax.swing.*;

public class Volume {
  double u[] = new double[3];
  double v[] = new double[3];
  double w[] = new double[3];
  double result;
  int counter;
  public Volume() {
  }

  public String menuCalculationVolumes(int number, double u1, double u2, double u3, double v1, double v2, double v3, double w1, double w2, double w3) {
    String finalScore = "";
    switch (number) {
      case 1: filledDataR3(u1, u2, u3, v1, v2, v3, w1, w2, w3);
        result = calculateVolume(u, v, w);
        finalScore += ("<THE RESULT IS --> " + result + " u3>");
        break;
      case 2: filledDataR3(u1, u2, u3, v1, v2, v3, w1, w2, w3);
        result = calculateVolume(u, v, w);
        finalScore += ("<THE RESULT IS --> " + (result /6) + " u3>");
        break;
  }
    return finalScore;
  }

  public void filledDataR3() {
    JOptionPane.showMessageDialog(null, "<Llenado de datos del primer vector --> 'u'>");
    counter = 120;
    for (int i = 0; i < 3; i++) {
      u[i] = Double.parseDouble(JOptionPane.showInputDialog(null, "<Digite el numero del elemento que se encuentra en la posicion " + numberACharacter(counter) + ">", "Solicitando datos", JOptionPane.ERROR_MESSAGE));
      counter++;
    }
    JOptionPane.showMessageDialog(null, "<Llenado de datos del segundo vector --> 'v'>");
    counter = 120;
    for (int i = 0; i < 3; i++) {
      v[i] = Double.parseDouble(JOptionPane.showInputDialog(null, "<Digite el numero del elemento que se encuentra en la posicion " + numberACharacter(counter) + ">", "Solicitando datos", JOptionPane.ERROR_MESSAGE));
      counter++;
    }
    JOptionPane.showMessageDialog(null, "<Llenado de datos del tercer vector --> 'w'>");
    counter = 120;
    for (int i = 0; i < 3; i++) {
      w[i] = Double.parseDouble(JOptionPane.showInputDialog(null, "<Digite el numero del elemento que se encuentra en la posicion " + numberACharacter(counter) + ">", "Solicitando datos", JOptionPane.ERROR_MESSAGE));
      counter++;
    }
  }

  public void filledDataR3(double u1, double u2, double u3, double v1, double v2, double v3, double w1, double w2, double w3) {
    u[0] = u1;
    u[1] = u2;
    u[2] = u3;
    v[0] = v1;
    v[1] = v2;
    v[2] = v3;
    w[0] = w1;
    w[1] = w2;
    w[2] = w3;
  }

  public double calculateVolume(double[] u, double[] v, double[] w) {
    double vectorProduct[] = new double[3];
    vectorProduct[0] = ((u[0] * v[1] * w[2]) + (v[0] * w[1] * u[2]) + (w[0] * u[1] * v[2]));
    vectorProduct[1] = ((u[2] * v[1] * w[0]) + (v[2] * w[1] * u[0]) + (w[2] * u[1] * v[0])) * -1;
    double product = vectorProduct[0] + vectorProduct[1];
    if (product < 0) {
      product *= -1;
    }
    double roundedProduct =  Math.round(product*100.0)/100.0;
    return roundedProduct;
  }

  public char numberACharacter(int number) {
    char letters = (char) number;
    return letters;
  }
}