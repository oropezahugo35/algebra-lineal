package algebra.lineal.functions;

import algebra.lineal.classes.Matrix;

import java.util.ArrayList;

/**
 * MatrixOperator class resolves matrix operations.
 */
public class MatrixOperator {

  private String name;
  private Matrix matrix1;
  private Matrix matrix2;

  public MatrixOperator(String name) {
    this.name = name;
  }

  public MatrixOperator(Matrix matrix1, Matrix matrix2, String name) {
    this.matrix1 = matrix1;
    this.matrix2 = matrix2;
    this.name = name;
  }

  /**
   * productOf method returns the product of the two matrix.
   */
  public Matrix productOf(){
    return MatrixVerifiers.verifyProduct(matrix1, matrix2) ? solveProduct() : new Matrix(this.name);
  }

  /**
   * additionOf method returns the result of the two matrix's addition.
   */
  public Matrix additionOf() {
    return (MatrixVerifiers.verifyAddition(matrix1, matrix2)) ? solveAddition() : new Matrix(this.name);
  }

  public void setMatrix1(Matrix matrix1) {
    this.matrix1 = matrix1;
  }

  public void setMatrix2(Matrix matrix2) {
    this.matrix2 = matrix2;
  }


  public Matrix getMatrix1() {
    return matrix1;
  }

  public Matrix getMatrix2() {
    return matrix2;
  }

  /**
   * solveProduct method resolve a matrix multiplication.
   */
  private Matrix solveProduct() {

      Matrix matrixResult = new Matrix(matrix1.getDimension1(), matrix2.getDimension2(), 0, this.name);
      ArrayList<Double> results = new ArrayList<>();
      double result = 0;

      for (int i = 0; i < matrixResult.getDimension1(); i++) {
        for (int j = 0; j < matrixResult.getDimension2(); j++) {
          double[] row = matrix1.getRow(i);
          double[] column = matrix2.getColumn(j);
          for (int k = 0; k < row.length; k++) {
            matrixResult.getMatrix()[i][j] += row[k] * column[k];
          }
        }
        results.add(result);
        result = 0;
      }

    return matrixResult;
  }

  /**
   * solveAddition method resolves a matrix addition.
   */
  private Matrix solveAddition() {
    Matrix matrixResult = new Matrix(matrix1.getDimension1(), matrix1.getDimension2(), 0, this.name);
    for(int i = 0; i < matrixResult.getDimension1(); i++) {
      for(int j = 0; j < matrixResult.getDimension2(); j++) {
        matrixResult.getMatrix()[i][j] = matrix1.getMatrix()[i][j] + matrix2.getMatrix()[i][j];
      }
    }
    return matrixResult;
  }
}
