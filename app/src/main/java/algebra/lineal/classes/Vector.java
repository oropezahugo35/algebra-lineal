package algebra.lineal.classes;

/**
 * Vector class abstracts a vector.
 */
public class Vector {
  private final double[] vector;
  private final int dimension;
  private final boolean isComponentVector;
  private final double[] initialPoint;
  private final double[] finalPoint;
  private final String name;
  private final boolean exists;
  private double vectorModule;
  private double[] vectorDirection;
  private boolean hasDirection;


  /**
   * constructor for cord vector.
   */
  public Vector( double[] initialPoint, double[] finalPoint, String name) {
    exists = initialPoint.length == finalPoint.length;
    this.dimension = initialPoint.length;
    this.vector = new double[dimension];
    this.isComponentVector = false;
    this.initialPoint = initialPoint;
    this.finalPoint = finalPoint;
    this.name = name;
    createVectorFromPoints();
    calculateVectorModule();
    setVectorDirectionAndHasDirection();

  }

  /**
   * constructor for component vector.
   */
  public Vector(double[] vector, String name) {
    exists = true;
    this.vector = vector;
    this.dimension = vector.length;
    this.isComponentVector = true;
    this.name = name;
    this.initialPoint = new double[dimension];
    this.finalPoint = new double[dimension];
    calculateVectorModule();
    setVectorDirectionAndHasDirection();
    createPointsFromVector();
  }

  /**
   * createVectorFromPoints method initializes a vector from points.
   */
  private void createVectorFromPoints() {
    if(exists) {
      for(int i = 0; i < dimension; i++) {
        vector[i] = finalPoint[i] - initialPoint[i];
      }
    }
  }

  /**
   * createPointsFromVector method initializes de points from a vector
   */
  private void createPointsFromVector() {
    for (int i = 0; i < dimension; i++) {
      initialPoint[i] = 0;
      finalPoint[i] = vector[i];
    }
  }

  /**
   * calculateVectorModule method calculates de vector's module.
   */
  private void calculateVectorModule() {
    int intern = 0;
    for (int i = 0; i < dimension; i++) {
      intern += Math.pow(vector[i], 2);
    }
    vectorModule = Math.sqrt(intern);
  }

  /**
   * calculateR2VectorDirection method calculates vector's direction for R2.
   */
  private void calculateR2VectorDirection() {
    if (vectorModule != 0.0) {
      vectorDirection[0] = Math.toDegrees(Math.atan(vector[1] / vector[0]));
    }
  }

  /**
   * calculateR3VectorDirection method calculates vector's direction for R3.
   */
  private void calculateR3VectorDirection() {
    if (vectorModule != 0.0) {
      vectorDirection[0] = Math.toDegrees(Math.acos(vector[0] / vectorModule));
      vectorDirection[1] = Math.toDegrees(Math.acos(vector[1] / vectorModule));
      vectorDirection[2] = Math.toDegrees(Math.acos(vector[2] / vectorModule));
    }
  }

  public double[] getVector() {
    return vector;
  }

  public int getDimension() {
    return dimension;
  }

  public boolean isComponentVector() {
    return isComponentVector;
  }

  public double[] getInitialPoint() {
    return initialPoint;
  }

  public double[] getFinalPoint() {
    return finalPoint;
  }

  public String getName() {
    return name;
  }

  public boolean isExists() {
    return exists;
  }

  public double getVectorModule() {
    return vectorModule;
  }

  public double[] getVectorDirection() {
    return vectorDirection;
  }

  public boolean isHasDirection() {
    return hasDirection;
  }

  /**
   * setVectorDirectionAndHasDirection method sets direction and hasDirection attributes.
   */
  private void setVectorDirectionAndHasDirection() {
    switch (dimension) {
      case 2:
        vectorDirection = new double[1];
        hasDirection = true;
        calculateR2VectorDirection();
        break;
      case 3:
        vectorDirection = new double[3];
        hasDirection = true;
        calculateR3VectorDirection();
        break;
      default:
        vectorDirection = new double[2];
        hasDirection = false;
        break;
    }
  }

  @Override
  public String toString() {
    String vector = name + " = ";
    if(exists) {
      if (isComponentVector) {
        vector += "(  ";
        for(int i = 0; i < this.vector.length; i++) {
          vector += i == dimension - 1 ? this.vector[i] + "  )": this.vector[i] + ", ";
        }
        vector += "\ntype = component" + "\nmodule = " + vectorModule + "\n" + "dimension = " + dimension + "\n";
        if (hasDirection) {
          if(dimension == 3) {
            vector += "alfa = " + vectorDirection[0] + "\n";
            vector += "beta = " + vectorDirection[1] + "\n";
            vector += "tita = " + vectorDirection[2] + "\n";
          } else {
            vector += "alfa = " + vectorDirection[0] + "\n";
          }
        } else {
          vector += "no calculable direction\n";
        }
      } else {
        vector += "(  ";
        for(int i = 0; i < this.initialPoint.length; i++) {
          vector += i == dimension - 1 ? this.initialPoint[i] + "  )": this.initialPoint[i] + ", ";
        }
        vector += "  -->  (  ";
        for(int i = 0; i < this.finalPoint.length; i++) {
          vector += i == dimension - 1 ? this.finalPoint[i] + "  )": this.finalPoint[i] + ", ";
        }
        vector += "\ntype = coordinate" + "\nmodule = " + vectorModule + "\ndimension = " + dimension + "\n";
        if (hasDirection) {
          if(dimension == 3) {
            vector += "alfa = " + vectorDirection[0] + "\n";
            vector += "beta = " + vectorDirection[1] + "\n";
            vector += "tita = " + vectorDirection[2] + "\n";
          } else {
            vector += "alfa = " + vectorDirection[0] + "\n";
          }
        } else {
          vector += "no calculable direction\n";
        }
      }
    } else {
      if(isComponentVector) {
        vector += "non-existent component vector";
      } else {
        vector += "non-existent coordinate vector";
      }

    }
    return vector;
  }
}
