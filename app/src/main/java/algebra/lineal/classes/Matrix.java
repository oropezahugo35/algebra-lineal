package algebra.lineal.classes;

import java.util.Random;

/**
 * Matrix class abstracts an algebra's matrix.
 */
public class Matrix {
  private final String name;
  private final int dimension1;
  private final int dimension2;
  private double[][] matrix;

  private final boolean exist;

  public Matrix(double[][] matrix, String name) {
    this.matrix = matrix;
    this.dimension1 = matrix.length;
    this.dimension2 = matrix[0].length;
    this.name = name;
    exist = true;
  }

  public Matrix(String name) {
    this.dimension1 = 1;
    this.dimension2 = 1;
    this.matrix = new double[dimension1][dimension2];
    randomMatrix();
    this.name = name;
    exist = false;
  }
  public Matrix(int dimension1, int dimension2, String name) {
    this.dimension1 = dimension1;
    this.dimension2 = dimension2;
    this.matrix = new double[dimension1][dimension2];
    randomMatrix();
    this.name = name;
    exist = true;
  }

  public Matrix(int dimension1, int dimension2, int num, String name) {
    this.dimension1 = dimension1;
    this.dimension2 = dimension2;
    this.matrix = new double[dimension1][dimension2];
    specifiedMatrix(num);
    this.name = name;
    exist = true;
  }

  /**
   * randomMatrix method initializes a random content matrix.
   */
  private void randomMatrix() {

    for (int i = 0; i < this.matrix.length; i++) {
      for(int j = 0; j < this.matrix[0].length; j++) {
        double num = (new Random().nextInt(10));
        boolean bool = new Random().nextBoolean();
        num *= (bool) ? -1: 1;
        this.matrix[i][j] = num;
      }
    }
  }

  /**
   * specifiedMatrix method initializes a matrix full of the specified number.
   */
  private void specifiedMatrix(int num) {
    for (int i = 0; i < this.matrix.length; i++) {
      for(int j = 0; j < this.matrix[0].length; j++) {
        this.matrix[i][j] = num;
      }
    }
  }

  /**
   * getDimension1 method returns the number of rows.
   */
  public int getDimension1() {
    return dimension1;
  }

  /**
   * getDimension2 method returns the number of columns.
   */
  public int getDimension2() {
    return dimension2;
  }

  /**
   * getMatrix method returns the matrix content.
   */
  public double[][] getMatrix() {
    return matrix;
  }

  /**
   * getName method returns the name of the matrix.
   */
  public String getName() {
    return name;
  }

  /**
   * getRow method returns the specified row's content.
   */
  public double[] getRow(int row) {
    double[] myRow = new double[dimension2];
    for (int i = 0; i < dimension2; i++) {
      myRow[i] = matrix[row][i];
    }
    return myRow;
  }

  /**
   * getColumn method returns the specified column's content.
   */
  public double[] getColumn(int column) {
    double[] myColumn = new double[dimension1];
    for (int i = 0; i < dimension1; i++) {
      myColumn[i] = matrix[i][column];
    }
    return myColumn;
  }

  public void setMatrixRow(int row, double[] myRow) {
    this.matrix[row] = myRow;
  }

  @Override
  public String toString() {
    if (exist) {
      String matrix = "";
      for (int i = 0; i < this.matrix.length; i++) {
        matrix += (i == dimension1 / 2) ? this.name + " = (  " : "\t  (  ";
        for (int j = 0; j < this.matrix[0].length; j++) {
          matrix += this.matrix[i][j] + "  ";
        }
        matrix += (i == this.matrix.length - 1) ? ")  " : ")\n";
      }

      return  matrix + dimension1 + " x " + dimension2 + "\n";
    } else {
      return this.name + " no existe\n";
    }
  }
}
