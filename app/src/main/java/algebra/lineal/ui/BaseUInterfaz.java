package algebra.lineal.ui;

import algebra.lineal.functions.Base;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import javax.swing.*;

public class BaseUInterfaz {
  public BaseUInterfaz() {
    initComponent();
  }

  private void initComponent() {
    Stage selectDifficulties = new Stage();
    Label cantidadVecotes = new Label("<Cuantos vectores tendra el plano>");
    TextField cantidadVecotesText = new TextField();
    Label espacioVectorial = new Label("<De que espacio vectorial seran los vectores>:");
    Label opcion = new Label("1.- R2 / 2.- R3 / 3.- R4 / 4.- R5 / 5.- R6");
    TextField espacioVectorialText = new TextField();
    Button continueButton = new Button("Continue");
    continueButton.setCursor(Cursor.HAND);
    continueButton.setStyle("-fx-border-color: #00264950; -fx-border-width: 5px; -fx-background-color: #FFFFFF;");
    continueButton.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.ITALIC, 15));
    Button exit = new Button("Exit");
    exit.setCursor(Cursor.HAND);
    exit.setStyle("-fx-border-color: #00264950; -fx-border-width: 5px; -fx-background-color: #FFFFFF;");
    exit.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.ITALIC, 15));
    //Label label = new Label("Digite un numero");
    //TextField textField = new TextField();
    //surface.setOnAction(event -> {System.out.println(textField.getText());});
    continueButton.setOnAction(event -> {
      Base base = new Base(Integer.parseInt(cantidadVecotesText.getText()), (Integer.parseInt(espacioVectorialText.getText()) + 1));
      if(!base.verificador()) {
          switch(Integer.parseInt(cantidadVecotesText.getText())) {
            case 1: initComponent1();
              break;
            case 2: initComponent2();
              break;
            case 3: initComponent3();
              break;
            case 4: initComponent4();
              break;
            case 5: initComponent5();
              break;
            case 6: initComponent6();
              break;
          }
      } else {
        switch(Integer.parseInt(cantidadVecotesText.getText())) {
          case 1: initComponent1();
            break;
          case 2: initComponent2p();
            break;
          case 3: initComponent3p();
            break;
          case 4: initComponent4p();
            break;
          case 5: initComponent5p();
            break;
          case 6: initComponent6p();
            break;
        }
      }
      });
    exit.setOnAction(event -> {selectDifficulties.close();});
    Group root = new Group();
    VBox box1 = new VBox();
    box1.getChildren().addAll(new Label(" "), cantidadVecotes, new Label(" "), cantidadVecotesText, new Label(" "), espacioVectorial, opcion, new Label(" "), espacioVectorialText, new Label(" "), continueButton, new Label(" "), exit);
    root.getChildren().addAll(box1);
    Scene scene = new Scene(root, 1280, 720);
    scene.setFill(Paint.valueOf("#D49E2A"));
    selectDifficulties.setScene(scene);
    //Stage close = (Stage) pane.getScene().getWindow();
    //close.close();
    selectDifficulties.setResizable(false);
    selectDifficulties.show();
  }

  private void initComponent1() {
    Stage selectDifficulties = new Stage();
    Label cantidadVecotes = new Label("<AGREGE ELEMENTOS AL VECTOR>");
    Label primerV = new Label("<Primer Vector>:");
    TextField primerVText = new TextField();
    Button calcular = new Button("Calculate");
    calcular.setCursor(Cursor.HAND);
    calcular.setStyle("-fx-border-color: #00264950; -fx-border-width: 5px; -fx-background-color: #FFFFFF;");
    calcular.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.ITALIC, 15));
    Button exit = new Button("Exit");
    exit.setCursor(Cursor.HAND);
    exit.setStyle("-fx-border-color: #00264950; -fx-border-width: 5px; -fx-background-color: #FFFFFF;");
    exit.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.ITALIC, 15));
    calcular.setOnAction(event -> {
      JOptionPane.showMessageDialog(null, "S are the given vectors, therefore S isn't a basis of V", "RESULT", JOptionPane.ERROR_MESSAGE);
     });
    exit.setOnAction(event -> {selectDifficulties.close();});
    Group root = new Group();
    VBox box1 = new VBox();
    box1.getChildren().addAll(new Label(" "), cantidadVecotes, new Label(" "), primerV, primerVText, new Label(" "), calcular, new Label(" "), exit);
    root.getChildren().addAll(box1);
    Scene scene = new Scene(root, 1280, 720);
    scene.setFill(Paint.valueOf("#D49E2A"));
    selectDifficulties.setScene(scene);
    //Stage close = (Stage) pane.getScene().getWindow();
    //close.close();
    selectDifficulties.setResizable(false);
    selectDifficulties.show();
  }

  private void initComponent2() {
    Stage selectDifficulties = new Stage();
    Label cantidadVecotes = new Label("<AGREGE ELEMENTOS AL VECTOR>");
    Label primerV = new Label("<Primer Vector>:");
    TextField primerVText = new TextField();
    Label segundoV = new Label("<Segundo Vector>:");
    TextField segundoVText = new TextField();
    Button calcular = new Button("Calculate");
    calcular.setCursor(Cursor.HAND);
    calcular.setStyle("-fx-border-color: #00264950; -fx-border-width: 5px; -fx-background-color: #FFFFFF;");
    calcular.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.ITALIC, 15));
    Button exit = new Button("Exit");
    exit.setCursor(Cursor.HAND);
    exit.setStyle("-fx-border-color: #00264950; -fx-border-width: 5px; -fx-background-color: #FFFFFF;");
    exit.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.ITALIC, 15));
    calcular.setOnAction(event -> {
      JOptionPane.showMessageDialog(null, "S are the given vectors, therefore S isn't a basis of V", "RESULT", JOptionPane.ERROR_MESSAGE);
    });
    exit.setOnAction(event -> {selectDifficulties.close();});
    Group root = new Group();
    VBox box1 = new VBox();
    box1.getChildren().addAll(new Label(" "), cantidadVecotes, new Label(" "), primerV, primerVText, new Label(" "), segundoV, segundoVText, new Label(" "), calcular, new Label(" "), exit);
    root.getChildren().addAll(box1);
    Scene scene = new Scene(root, 1280, 720);
    scene.setFill(Paint.valueOf("#D49E2A"));
    selectDifficulties.setScene(scene);
    //Stage close = (Stage) pane.getScene().getWindow();
    //close.close();
    selectDifficulties.setResizable(false);
    selectDifficulties.show();
  }

  private void initComponent3() {
    Stage selectDifficulties = new Stage();
    Label cantidadVecotes = new Label("<AGREGE ELEMENTOS AL VECTOR>");
    Label primerV = new Label("<Primer Vector>:");
    TextField primerVText = new TextField();
    Label segundoV = new Label("<Segundo Vector>:");
    TextField segundoVText = new TextField();
    Label tercerV = new Label("<Tercer Vector>:");
    TextField tercerVText = new TextField();
    Button calcular = new Button("Calculate");
    calcular.setCursor(Cursor.HAND);
    calcular.setStyle("-fx-border-color: #00264950; -fx-border-width: 5px; -fx-background-color: #FFFFFF;");
    calcular.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.ITALIC, 15));
    Button exit = new Button("Exit");
    exit.setCursor(Cursor.HAND);
    exit.setStyle("-fx-border-color: #00264950; -fx-border-width: 5px; -fx-background-color: #FFFFFF;");
    exit.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.ITALIC, 15));
    calcular.setOnAction(event -> {
      JOptionPane.showMessageDialog(null, "S are the given vectors, therefore S isn't a basis of V", "RESULT", JOptionPane.ERROR_MESSAGE);
    });
    exit.setOnAction(event -> {selectDifficulties.close();});
    Group root = new Group();
    VBox box1 = new VBox();
    box1.getChildren().addAll(new Label(" "), cantidadVecotes, new Label(" "), primerV, primerVText, new Label(" "), segundoV, segundoVText, new Label(" "), tercerV, tercerVText, new Label(" "), calcular, new Label(" "), exit);
    root.getChildren().addAll(box1);
    Scene scene = new Scene(root, 1280, 720);
    scene.setFill(Paint.valueOf("#D49E2A"));
    selectDifficulties.setScene(scene);
    //Stage close = (Stage) pane.getScene().getWindow();
    //close.close();
    selectDifficulties.setResizable(false);
    selectDifficulties.show();
  }

  private void initComponent4() {
    Stage selectDifficulties = new Stage();
    Label cantidadVecotes = new Label("<AGREGE ELEMENTOS AL VECTOR>");
    Label primerV = new Label("<Primer Vector>:");
    TextField primerVText = new TextField();
    Label segundoV = new Label("<Segundo Vector>:");
    TextField segundoVText = new TextField();
    Label tercerV = new Label("<Tercer Vector>:");
    TextField tercerVText = new TextField();
    Label cuartoV = new Label("<Cuarto Vector>:");
    TextField cuartoVText = new TextField();
    Button calcular = new Button("Calculate");
    calcular.setCursor(Cursor.HAND);
    calcular.setStyle("-fx-border-color: #00264950; -fx-border-width: 5px; -fx-background-color: #FFFFFF;");
    calcular.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.ITALIC, 15));
    Button exit = new Button("Exit");
    exit.setCursor(Cursor.HAND);
    exit.setStyle("-fx-border-color: #00264950; -fx-border-width: 5px; -fx-background-color: #FFFFFF;");
    exit.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.ITALIC, 15));
    calcular.setOnAction(event -> {
      JOptionPane.showMessageDialog(null, "S are the given vectors, therefore S isn't a basis of V", "RESULT", JOptionPane.ERROR_MESSAGE);
    });
    exit.setOnAction(event -> {selectDifficulties.close();});
    Group root = new Group();
    VBox box1 = new VBox();
    box1.getChildren().addAll(new Label(" "), cantidadVecotes, new Label(" "), primerV, primerVText, new Label(" "), segundoV, segundoVText, new Label(" "), tercerV, tercerVText, new Label(" "), cuartoV, cuartoVText, new Label(" "), calcular, new Label(" "), exit);
    root.getChildren().addAll(box1);
    Scene scene = new Scene(root, 1280, 720);
    scene.setFill(Paint.valueOf("#D49E2A"));
    selectDifficulties.setScene(scene);
    //Stage close = (Stage) pane.getScene().getWindow();
    //close.close();
    selectDifficulties.setResizable(false);
    selectDifficulties.show();
  }

  private void initComponent5() {
    Stage selectDifficulties = new Stage();
    Label cantidadVecotes = new Label("<AGREGE ELEMENTOS AL VECTOR>");
    Label primerV = new Label("<Primer Vector>:");
    TextField primerVText = new TextField();
    Label segundoV = new Label("<Segundo Vector>:");
    TextField segundoVText = new TextField();
    Label tercerV = new Label("<Tercer Vector>:");
    TextField tercerVText = new TextField();
    Label cuartoV = new Label("<Cuarto Vector>:");
    TextField cuartoVText = new TextField();
    Label quintoV = new Label("<Quinto Vector>:");
    TextField quintoVText = new TextField();
    Button calcular = new Button("Calculate");
    calcular.setCursor(Cursor.HAND);
    calcular.setStyle("-fx-border-color: #00264950; -fx-border-width: 5px; -fx-background-color: #FFFFFF;");
    calcular.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.ITALIC, 15));
    Button exit = new Button("Exit");
    exit.setCursor(Cursor.HAND);
    exit.setStyle("-fx-border-color: #00264950; -fx-border-width: 5px; -fx-background-color: #FFFFFF;");
    exit.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.ITALIC, 15));
    calcular.setOnAction(event -> {
      JOptionPane.showMessageDialog(null, "S are the given vectors, therefore S isn't a basis of V", "RESULT", JOptionPane.ERROR_MESSAGE);
    });
    exit.setOnAction(event -> {selectDifficulties.close();});
    Group root = new Group();
    VBox box1 = new VBox();
    box1.getChildren().addAll(new Label(" "), cantidadVecotes, new Label(" "), primerV, primerVText, new Label(" "), segundoV, segundoVText, new Label(" "), tercerV, tercerVText, new Label(" "), cuartoV, cuartoVText, new Label(" "), quintoV, quintoVText, new Label(" "), calcular, new Label(" "), exit);
    root.getChildren().addAll(box1);
    Scene scene = new Scene(root, 1280, 720);
    scene.setFill(Paint.valueOf("#D49E2A"));
    selectDifficulties.setScene(scene);
    //Stage close = (Stage) pane.getScene().getWindow();
    //close.close();
    selectDifficulties.setResizable(false);
    selectDifficulties.show();
  }

  private void initComponent6() {
    Stage selectDifficulties = new Stage();
    Label cantidadVecotes = new Label("<AGREGE ELEMENTOS AL VECTOR>");
    Label primerV = new Label("<Primer Vector>:");
    TextField primerVText = new TextField();
    Label segundoV = new Label("<Segundo Vector>:");
    TextField segundoVText = new TextField();
    Label tercerV = new Label("<Tercer Vector>:");
    TextField tercerVText = new TextField();
    Label cuartoV = new Label("<Cuarto Vector>:");
    TextField cuartoVText = new TextField();
    Label quintoV = new Label("<Quinto Vector>:");
    TextField quintoVText = new TextField();
    Label sextoV = new Label("<Sexto Vector>:");
    TextField sextoVText = new TextField();
    Button calcular = new Button("Calculate");
    calcular.setCursor(Cursor.HAND);
    calcular.setStyle("-fx-border-color: #00264950; -fx-border-width: 5px; -fx-background-color: #FFFFFF;");
    calcular.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.ITALIC, 15));
    Button exit = new Button("Exit");
    exit.setCursor(Cursor.HAND);
    exit.setStyle("-fx-border-color: #00264950; -fx-border-width: 5px; -fx-background-color: #FFFFFF;");
    exit.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.ITALIC, 15));
    calcular.setOnAction(event -> {
      JOptionPane.showMessageDialog(null, "S are the given vectors, therefore S isn't a basis of V", "RESULT", JOptionPane.ERROR_MESSAGE);
    });
    exit.setOnAction(event -> {selectDifficulties.close();});
    Group root = new Group();
    VBox box1 = new VBox();
    box1.getChildren().addAll(new Label(" "), cantidadVecotes, new Label(" "), primerV, primerVText, new Label(" "), segundoV, segundoVText, new Label(" "), tercerV, tercerVText, new Label(" "), cuartoV, cuartoVText, new Label(" "), quintoV, quintoVText, new Label(" "), sextoV, segundoVText, new Label(" "), calcular, new Label(" "), exit);
    root.getChildren().addAll(box1);
    Scene scene = new Scene(root, 1280, 720);
    scene.setFill(Paint.valueOf("#D49E2A"));
    selectDifficulties.setScene(scene);
    //Stage close = (Stage) pane.getScene().getWindow();
    //close.close();
    selectDifficulties.setResizable(false);
    selectDifficulties.show();
  }

  private void initComponent2p() {
    Stage selectDifficulties = new Stage();
    Label cantidadVecotes = new Label("<AGREGE ELEMENTOS AL VECTOR>");
    Label primerV = new Label("<Primer Vector>:");
    TextField primerVText = new TextField();
    Label segundoV = new Label("<Segundo Vector>:");
    TextField segundoVText = new TextField();
    Button calcular = new Button("Calculate");
    calcular.setCursor(Cursor.HAND);
    calcular.setStyle("-fx-border-color: #00264950; -fx-border-width: 5px; -fx-background-color: #FFFFFF;");
    calcular.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.ITALIC, 15));
    Button exit = new Button("Exit");
    exit.setCursor(Cursor.HAND);
    exit.setStyle("-fx-border-color: #00264950; -fx-border-width: 5px; -fx-background-color: #FFFFFF;");
    exit.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.ITALIC, 15));
    calcular.setOnAction(event -> {
      String valores1 = primerVText.getText();
      String[] lista1 = valores1.split(",");
      String valores2 = segundoVText.getText();
      String[] lista2 = valores2.split(",");
      Base base = new Base();
      String valor = base.isBase2(Double.parseDouble(lista1[0]), Double.parseDouble(lista1[1]), Double.parseDouble(lista2[0]), Double.parseDouble(lista2[0]));
      JOptionPane.showMessageDialog(null, valor, "RESULT", JOptionPane.ERROR_MESSAGE);
    });
    exit.setOnAction(event -> {selectDifficulties.close();});
    Group root = new Group();
    VBox box1 = new VBox();
    box1.getChildren().addAll(new Label(" "), cantidadVecotes, new Label(" "), primerV, primerVText, new Label(" "), segundoV, segundoVText, new Label(" "), calcular, new Label(" "), exit);
    root.getChildren().addAll(box1);
    Scene scene = new Scene(root, 1280, 720);
    scene.setFill(Paint.valueOf("#D49E2A"));
    selectDifficulties.setScene(scene);
    //Stage close = (Stage) pane.getScene().getWindow();
    //close.close();
    selectDifficulties.setResizable(false);
    selectDifficulties.show();
  }

  private void initComponent3p() {
    Stage selectDifficulties = new Stage();
    Label cantidadVecotes = new Label("<AGREGE ELEMENTOS AL VECTOR>");
    Label primerV = new Label("<Primer Vector>:");
    TextField primerVText = new TextField();
    Label segundoV = new Label("<Segundo Vector>:");
    TextField segundoVText = new TextField();
    Label tercerV = new Label("<Tercer Vector>:");
    TextField tercerVText = new TextField();
    Button calcular = new Button("Calculate");
    calcular.setCursor(Cursor.HAND);
    calcular.setStyle("-fx-border-color: #00264950; -fx-border-width: 5px; -fx-background-color: #FFFFFF;");
    calcular.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.ITALIC, 15));
    Button exit = new Button("Exit");
    exit.setCursor(Cursor.HAND);
    exit.setStyle("-fx-border-color: #00264950; -fx-border-width: 5px; -fx-background-color: #FFFFFF;");
    exit.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.ITALIC, 15));
    calcular.setOnAction(event -> {
      String valores1 = primerVText.getText();
      String[] lista1 = valores1.split(",");
      String valores2 = segundoVText.getText();
      String[] lista2 = valores2.split(",");
      String valores3 = tercerVText.getText();
      String[] lista3 = valores3.split(",");
      Base base = new Base();
      double[] l1b = new double[3];
      double[] l2b = new double[3];
      double[] l3b = new double[3];
      for (int i = 0; i < 3; i++) {
        l1b[i] = Double.parseDouble(lista1[i]);
      }
      for (int i = 0; i < 3; i++) {
        l2b[i] = Double.parseDouble(lista2[i]);
      }
      for (int i = 0; i < 3; i++) {
        l3b[i] = Double.parseDouble(lista3[i]);
      }
      String valor = base.isBase3(l1b[0], l1b[1], l1b[2], l2b[0], l2b[1], l2b[2], l3b[0], l3b[1], l3b[2]);
      JOptionPane.showMessageDialog(null, valor, "RESULT", JOptionPane.ERROR_MESSAGE);
        });
    exit.setOnAction(event -> {selectDifficulties.close();});
    Group root = new Group();
    VBox box1 = new VBox();
    box1.getChildren().addAll(new Label(" "), cantidadVecotes, new Label(" "), primerV, primerVText, new Label(" "), segundoV, segundoVText, new Label(" "), tercerV, tercerVText, new Label(" "), calcular, new Label(" "), exit);
    root.getChildren().addAll(box1);
    Scene scene = new Scene(root, 1280, 720);
    scene.setFill(Paint.valueOf("#D49E2A"));
    selectDifficulties.setScene(scene);
    //Stage close = (Stage) pane.getScene().getWindow();
    //close.close();
    selectDifficulties.setResizable(false);
    selectDifficulties.show();
  }

  private void initComponent4p() {
    Stage selectDifficulties = new Stage();
    Label cantidadVecotes = new Label("<AGREGE ELEMENTOS AL VECTOR>");
    Label primerV = new Label("<Primer Vector>:");
    TextField primerVText = new TextField();
    Label segundoV = new Label("<Segundo Vector>:");
    TextField segundoVText = new TextField();
    Label tercerV = new Label("<Tercer Vector>:");
    TextField tercerVText = new TextField();
    Label cuartoV = new Label("<Cuarto Vector>:");
    TextField cuartoVText = new TextField();
    Button calcular = new Button("Calculate");
    calcular.setCursor(Cursor.HAND);
    calcular.setStyle("-fx-border-color: #00264950; -fx-border-width: 5px; -fx-background-color: #FFFFFF;");
    calcular.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.ITALIC, 15));
    Button exit = new Button("Exit");
    exit.setCursor(Cursor.HAND);
    exit.setStyle("-fx-border-color: #00264950; -fx-border-width: 5px; -fx-background-color: #FFFFFF;");
    exit.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.ITALIC, 15));
    calcular.setOnAction(event -> {
      String valores1 = primerVText.getText();
      String[] lista1 = valores1.split(",");
      String valores2 = segundoVText.getText();
      String[] lista2 = valores2.split(",");
      String valores3 = tercerVText.getText();
      String[] lista3 = valores3.split(",");
      String valores4 = cuartoVText.getText();
      String[] lista4 = valores4.split(",");
      Base base = new Base();
      double[] l1b = new double[4];
      double[] l2b = new double[4];
      double[] l3b = new double[4];
      double[] l4b = new double[4];
      for (int i = 0; i < 4; i++) {
        l1b[i] = Double.parseDouble(lista1[i]);
      }
      for (int i = 0; i < 4; i++) {
        l2b[i] = Double.parseDouble(lista2[i]);
      }
      for (int i = 0; i < 4; i++) {
        l3b[i] = Double.parseDouble(lista3[i]);
      }
      for (int i = 0; i < 4; i++) {
        l4b[i] = Double.parseDouble(lista4[i]);
      }
      String valor = base.isBase4(l1b[0], l1b[1], l1b[2], l1b[3], l2b[0], l2b[1], l2b[2], l2b[3], l3b[0], l3b[1], l3b[2], l3b[3], l4b[0], l4b[1], l4b[2], l4b[3]);
      JOptionPane.showMessageDialog(null, valor, "RESULT", JOptionPane.ERROR_MESSAGE);
    });
    exit.setOnAction(event -> {selectDifficulties.close();});
    Group root = new Group();
    VBox box1 = new VBox();
    box1.getChildren().addAll(new Label(" "), cantidadVecotes, new Label(" "), primerV, primerVText, new Label(" "), segundoV, segundoVText, new Label(" "), tercerV, tercerVText, new Label(" "), cuartoV, cuartoVText, new Label(" "), calcular, new Label(" "), exit);
    root.getChildren().addAll(box1);
    Scene scene = new Scene(root, 1280, 720);
    scene.setFill(Paint.valueOf("#D49E2A"));
    selectDifficulties.setScene(scene);
    //Stage close = (Stage) pane.getScene().getWindow();
    //close.close();
    selectDifficulties.setResizable(false);
    selectDifficulties.show();
  }

  private void initComponent5p() {
    Stage selectDifficulties = new Stage();
    Label cantidadVecotes = new Label("<AGREGE ELEMENTOS AL VECTOR>");
    Label primerV = new Label("<Primer Vector>:");
    TextField primerVText = new TextField();
    Label segundoV = new Label("<Segundo Vector>:");
    TextField segundoVText = new TextField();
    Label tercerV = new Label("<Tercer Vector>:");
    TextField tercerVText = new TextField();
    Label cuartoV = new Label("<Cuarto Vector>:");
    TextField cuartoVText = new TextField();
    Label quintoV = new Label("<Quinto Vector>:");
    TextField quintoVText = new TextField();
    Button calcular = new Button("Calculate");
    calcular.setCursor(Cursor.HAND);
    calcular.setStyle("-fx-border-color: #00264950; -fx-border-width: 5px; -fx-background-color: #FFFFFF;");
    calcular.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.ITALIC, 15));
    Button exit = new Button("Exit");
    exit.setCursor(Cursor.HAND);
    exit.setStyle("-fx-border-color: #00264950; -fx-border-width: 5px; -fx-background-color: #FFFFFF;");
    exit.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.ITALIC, 15));
    calcular.setOnAction(event -> {
      String valores1 = primerVText.getText();
      String[] lista1 = valores1.split(",");
      String valores2 = segundoVText.getText();
      String[] lista2 = valores2.split(",");
      String valores3 = tercerVText.getText();
      String[] lista3 = valores3.split(",");
      String valores4 = cuartoVText.getText();
      String[] lista4 = valores4.split(",");
      String valores5 = quintoVText.getText();
      String[] lista5 = valores5.split(",");
      Base base = new Base();
      double[] l1b = new double[5];
      double[] l2b = new double[5];
      double[] l3b = new double[5];
      double[] l4b = new double[5];
      double[] l5b = new double[5];
      for (int i = 0; i < 5; i++) {
        l1b[i] = Double.parseDouble(lista1[i]);
      }
      for (int i = 0; i < 5; i++) {
        l2b[i] = Double.parseDouble(lista2[i]);
      }
      for (int i = 0; i < 5; i++) {
        l3b[i] = Double.parseDouble(lista3[i]);
      }
      for (int i = 0; i < 5; i++) {
        l4b[i] = Double.parseDouble(lista4[i]);
      }
      for (int i = 0; i < 5; i++) {
        l5b[i] = Double.parseDouble(lista5[i]);
      }
      String valor = base.isBase5(l1b[0], l1b[1], l1b[2], l1b[3], l1b[4], l2b[0], l2b[1], l2b[2], l2b[3], l2b[4], l3b[0], l3b[1], l3b[2], l3b[3], l3b[4], l4b[0], l4b[1], l4b[2], l4b[3], l4b[4], l5b[0], l5b[1], l5b[2], l5b[3], l5b[4]);
      JOptionPane.showMessageDialog(null, valor, "RESULT", JOptionPane.ERROR_MESSAGE);
    });
    exit.setOnAction(event -> {selectDifficulties.close();});
    Group root = new Group();
    VBox box1 = new VBox();
    box1.getChildren().addAll(new Label(" "), cantidadVecotes, new Label(" "), primerV, primerVText, new Label(" "), segundoV, segundoVText, new Label(" "), tercerV, tercerVText, new Label(" "), cuartoV, cuartoVText, new Label(" "), quintoV, quintoVText, new Label(" "), calcular, new Label(" "), exit);
    root.getChildren().addAll(box1);
    Scene scene = new Scene(root, 1280, 720);
    scene.setFill(Paint.valueOf("#D49E2A"));
    selectDifficulties.setScene(scene);
    //Stage close = (Stage) pane.getScene().getWindow();
    //close.close();
    selectDifficulties.setResizable(false);
    selectDifficulties.show();
  }

  private void initComponent6p() {
    Stage selectDifficulties = new Stage();
    Label cantidadVecotes = new Label("<AGREGE ELEMENTOS AL VECTOR>");
    Label primerV = new Label("<Primer Vector>:");
    TextField primerVText = new TextField();
    Label segundoV = new Label("<Segundo Vector>:");
    TextField segundoVText = new TextField();
    Label tercerV = new Label("<Tercer Vector>:");
    TextField tercerVText = new TextField();
    Label cuartoV = new Label("<Cuarto Vector>:");
    TextField cuartoVText = new TextField();
    Label quintoV = new Label("<Quinto Vector>:");
    TextField quintoVText = new TextField();
    Label sextoV = new Label("<Sexto Vector>:");
    TextField sextoVText = new TextField();
    Button calcular = new Button("Calculate");
    calcular.setCursor(Cursor.HAND);
    calcular.setStyle("-fx-border-color: #00264950; -fx-border-width: 5px; -fx-background-color: #FFFFFF;");
    calcular.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.ITALIC, 15));
    Button exit = new Button("Exit");
    exit.setCursor(Cursor.HAND);
    exit.setStyle("-fx-border-color: #00264950; -fx-border-width: 5px; -fx-background-color: #FFFFFF;");
    exit.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.ITALIC, 15));
    calcular.setOnAction(event -> {
      String valores1 = primerVText.getText();
      String[] lista1 = valores1.split(",");
      String valores2 = segundoVText.getText();
      String[] lista2 = valores2.split(",");
      String valores3 = tercerVText.getText();
      String[] lista3 = valores3.split(",");
      String valores4 = cuartoVText.getText();
      String[] lista4 = valores4.split(",");
      String valores5 = quintoVText.getText();
      String[] lista5 = valores5.split(",");
      String valores6 = sextoVText.getText();
      String[] lista6 = valores6.split(",");
      Base base = new Base();
      double[] l1b = new double[6];
      double[] l2b = new double[6];
      double[] l3b = new double[6];
      double[] l4b = new double[6];
      double[] l5b = new double[6];
      double[] l6b = new double[6];
      for (int i = 0; i < 6; i++) {
        l1b[i] = Double.parseDouble(lista1[i]);
      }
      for (int i = 0; i < 6; i++) {
        l2b[i] = Double.parseDouble(lista2[i]);
      }
      for (int i = 0; i < 6; i++) {
        l3b[i] = Double.parseDouble(lista3[i]);
      }
      for (int i = 0; i < 6; i++) {
        l4b[i] = Double.parseDouble(lista4[i]);
      }
      for (int i = 0; i < 6; i++) {
        l5b[i] = Double.parseDouble(lista5[i]);
      }
      for (int i = 0; i < 6; i++) {
        l6b[i] = Double.parseDouble(lista6[i]);
      }
      String valor = base.isBase6(l1b[0], l1b[1], l1b[2], l1b[3], l1b[4], l1b[5], l2b[0], l2b[1], l2b[2], l2b[3], l2b[4], l2b[5], l3b[0], l3b[1], l3b[2], l3b[3], l3b[4], l3b[5], l4b[0], l4b[1], l4b[2], l4b[3], l4b[4], l4b[5], l5b[0], l5b[1], l5b[2], l5b[3], l5b[4], l5b[5], l6b[0], l6b[1], l6b[2], l6b[3], l6b[4], l6b[5]);
      JOptionPane.showMessageDialog(null, valor, "RESULT", JOptionPane.ERROR_MESSAGE);
      });
    exit.setOnAction(event -> {selectDifficulties.close();});
    Group root = new Group();
    VBox box1 = new VBox();
    box1.getChildren().addAll(new Label(" "), cantidadVecotes, new Label(" "), primerV, primerVText, new Label(" "), segundoV, segundoVText, new Label(" "), tercerV, tercerVText, new Label(" "), cuartoV, cuartoVText, new Label(" "), quintoV, quintoVText, new Label(" "), sextoV, segundoVText, new Label(" "), calcular, new Label(" "), exit);
    root.getChildren().addAll(box1);
    Scene scene = new Scene(root, 1280, 720);
    scene.setFill(Paint.valueOf("#D49E2A"));
    selectDifficulties.setScene(scene);
    //Stage close = (Stage) pane.getScene().getWindow();
    //close.close();
    selectDifficulties.setResizable(false);
    selectDifficulties.show();
  }
}
