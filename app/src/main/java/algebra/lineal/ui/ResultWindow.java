package algebra.lineal.ui;

import algebra.lineal.classes.Matrix;
import algebra.lineal.common.ComposableComponent;
import algebra.lineal.functions.MatrixOperator;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import static algebra.lineal.JavaFX.buildPane;

public class ResultWindow implements ComposableComponent {

  private final Pane pane;
  private final Button exit;
  private final Label result;
  private final VBox vbox;
  private final Button backMatrixInsert;
  public MatrixOperator operator;
  public boolean product;

  public ResultWindow(MatrixOperator matrixOperator1, boolean bool) {
    this.operator = matrixOperator1;
    this.pane = new Pane();
    this.exit = new Button("exit");
    this.backMatrixInsert = new Button("back");
    this.product = bool;
    this.result = new Label(getResult());
    this.vbox = new VBox();
  }

  public MatrixOperator getOperator() {
    return operator;
  }

  public String getResult() {
    if(product) {
      Matrix matrix = operator.productOf();
      return operator.getMatrix1().toString() + "\n" + operator.getMatrix2().toString() + "\n" + matrix.toString();
    } else {
      Matrix matrix = operator.additionOf();
      return operator.getMatrix1().toString() + "\n" + operator.getMatrix2().toString() + "\n" + matrix;
    }

  }
  public void setOperator(MatrixOperator operator) {
    this.operator = operator;
  }

  @Override
  public void configureComponent() {

  }

  @Override
  public Node getComponent() {
    return this.pane;
  }

  @Override
  public void compose() {
    backMatrixInsert.setOnAction(this::size);
    exit.setOnAction(this::exit);
    vbox.getChildren().addAll(result, exit, backMatrixInsert);
    pane.getChildren().add(vbox);
  }

  public void exit(ActionEvent open) {
    Stage stage = (Stage) pane.getScene().getWindow();
    stage.close();
  }

  public void size(ActionEvent open) {
    Stage selectDifficulties = new Stage();
    Scene scene = new Scene(buildPane(new SizeMenu()),1280,720);
    selectDifficulties.setScene(scene);
    Stage close = (Stage) pane.getScene().getWindow();
    close.close();
    selectDifficulties.setResizable(false);
    selectDifficulties.show();
  }
}
