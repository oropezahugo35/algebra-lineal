package algebra.lineal.ui;

import algebra.lineal.classes.Matrix;
import algebra.lineal.common.ComposableComponent;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import static algebra.lineal.JavaFX.buildPane;

public class SizeMenu implements ComposableComponent {

  private final Pane pane;
  private final SingleCharacterTextField inputRowsOfMatrix1;
  private final SingleCharacterTextField inputRowsOfMatrix2;
  private final Label label1;
  private final SingleCharacterTextField inputColumnsOfMatrix1;
  private final SingleCharacterTextField inputColumnsOfMatrix2;
  private final Label label2;
  private final VBox vbox;
  private final Button button;
  private final SingleCharacterTextField inputName1;
  private final SingleCharacterTextField inputName2;
  private Matrix matrix1;
  private Matrix matrix2;


  public SizeMenu() {
    this.pane = new Pane();
    this.inputName1 = new SingleCharacterTextField();
    this.inputName2 = new SingleCharacterTextField();
    this.inputRowsOfMatrix1 = new SingleCharacterTextField();
    this.inputRowsOfMatrix2 = new SingleCharacterTextField();
    this.label1 = new Label("rows");
    this.inputColumnsOfMatrix1 = new SingleCharacterTextField();
    this.inputColumnsOfMatrix2 = new SingleCharacterTextField();
    this.label2 = new Label("columns");
    this.vbox = new VBox();
    this.button = new Button("continue");
  }

  @Override
  public void configureComponent() {
    pane.setPrefHeight(720);
    pane.setPrefWidth(1280);
    pane.setStyle("-fx-background-color: A7FBF1");
  }

  @Override
  public Node getComponent() {
    return this.pane;
  }

  @Override
  public void compose() {
    button.setOnAction(event -> {
      this.matrix1 = new Matrix(Integer.parseInt(this.inputRowsOfMatrix1.getText()), Integer.parseInt(this.inputColumnsOfMatrix1.getText()), this.inputName1.getText());
      this.matrix2 = new Matrix(Integer.parseInt(this.inputRowsOfMatrix2.getText()), Integer.parseInt(this.inputColumnsOfMatrix2.getText()), this.inputName2.getText());
      Stage selectDifficulties = new Stage();
      Scene scene = new Scene(buildPane(new SetMatrixesMenu(matrix1, matrix2)),1280,720);
      selectDifficulties.setScene(scene);
      Stage close = (Stage) pane.getScene().getWindow();
      close.close();
      selectDifficulties.setResizable(false);
      selectDifficulties.show();
    });

    vbox.getChildren().addAll( new Label("name1"), inputName1 ,label1, inputRowsOfMatrix1,
            label2, inputColumnsOfMatrix1, new Label("name2"), inputName2, new Label("rows"),
            inputRowsOfMatrix2, new Label("columns"), inputColumnsOfMatrix2,  button);
    pane.getChildren().add(vbox);
  }
}
