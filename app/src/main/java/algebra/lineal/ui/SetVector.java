package algebra.lineal.ui;

import algebra.lineal.classes.Vector;
import algebra.lineal.common.ComposableComponent;
import algebra.lineal.functions.StringManager;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import static algebra.lineal.JavaFX.buildPane;

public class SetVector implements ComposableComponent {
  private final Pane pane;
  private final VBox vBox;
  private final TextField inputSize;
  private final TextField inputName;
  private final TextField inputBase;
  private final TextField inputSurfaceAndVolume;
  private final TextField inputModuleAndDirection;
  private final TextField inputSurface;
  private final TextField inputVolume;
  private final Button calculateBase;
  private final Button calculateSurfaceAndVolume;
  private final Button calculateModuleAndDirection;
  private final Button calculateModule;
  private final Button calculateDirection;
  private final Button calculateSurface;
  private final Button calculateVolume;

  public SetVector() {
    this.pane = new Pane();
    this.vBox = new VBox();
    this.inputSize = new TextField();
    this.inputName = new TextField();
    this.inputBase = new TextField();
    this.inputSurfaceAndVolume = new TextField();
    this.inputModuleAndDirection = new TextField();
    this.inputSurface = new TextField();
    this.inputVolume = new TextField();
    this.calculateBase= new Button("Base");
    this.calculateSurfaceAndVolume = new Button("Surface and Volume");
    this.calculateModuleAndDirection = new Button("Module and direction");
    this.calculateModule = new Button("module");
    this.calculateDirection = new Button("direction");
    this.calculateSurface = new Button("surface");
    this.calculateVolume = new Button("volume");
  }

  @Override
  public void configureComponent() {
    pane.setPrefHeight(720);
    pane.setPrefWidth(1280);
    pane.setStyle("-fx-background-color: A7FBF1");
  }

  @Override
  public Node getComponent() {
    return pane;
  }


  public void getModule(Button button) {
    button.setOnAction(event -> {
      Vector vector = new Vector(new StringManager(inputSize.getText()).convertToVector(), inputName.getText());
      Stage selectDifficulties = new Stage();
      Scene scene = new Scene(buildPane(new ResultVectorWindow(vector, 0)),1280,720);
      selectDifficulties.setScene(scene);
      Stage close = (Stage) pane.getScene().getWindow();
      close.close();
      selectDifficulties.setResizable(false);
      selectDifficulties.show();
    });
  }

  public void getDirection(Button button) {
    button.setOnAction(event -> {
      Vector vector = new Vector(new StringManager(inputSize.getText()).convertToVector(), inputName.getText());
      Stage selectDifficulties = new Stage();
      Scene scene = new Scene(buildPane(new ResultVectorWindow(vector, 1)),1280,720);
      selectDifficulties.setScene(scene);
      Stage close = (Stage) pane.getScene().getWindow();
      scene.setFill(Paint.valueOf("#D49E2A"));
      close.close();
      selectDifficulties.setResizable(false);
      selectDifficulties.show();
    });
  }

  public void getSurfaceAndVolume(Button button) {
    button.setOnAction(event -> {
      SufaceAndVolumeWindows obj = new SufaceAndVolumeWindows();
    });
  }

  public void getBase(Button button) {
    button.setOnAction(event -> {
      BaseUInterfaz obj = new BaseUInterfaz();
    });
  }

  public void getModuleAndDirection(Button button) {
    button.setOnAction(event -> {
      ModuleAndDirectionUI obj = new ModuleAndDirectionUI();
    });
  }
  @Override
  public void compose() {
    //getModule(calculateModule);
    //getDirection(calculateDirection);
    Label titulo = new Label("<Choose the operation you want to perform>");
    titulo.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.ITALIC, 35));
    calculateSurfaceAndVolume.setCursor(Cursor.HAND);
    calculateSurfaceAndVolume.setStyle("-fx-border-color: #00264950; -fx-border-width: 5px; -fx-background-color: #FFFFFF;");
    calculateSurfaceAndVolume.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.ITALIC, 15));
    calculateBase.setCursor(Cursor.HAND);
    calculateBase.setStyle("-fx-border-color: #00264950; -fx-border-width: 5px; -fx-background-color: #FFFFFF;");
    calculateBase.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.ITALIC, 15));
    calculateModuleAndDirection.setCursor(Cursor.HAND);
    calculateModuleAndDirection.setStyle("-fx-border-color: #00264950; -fx-border-width: 5px; -fx-background-color: #FFFFFF;");
    calculateModuleAndDirection.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.ITALIC, 15));
    getBase(calculateBase);
    getSurfaceAndVolume(calculateSurfaceAndVolume);
    getModuleAndDirection(calculateModuleAndDirection);
    //vBox.getChildren().addAll(new Label("Name"), inputName, new Label("Insert vector"), inputSize, calculateModule, calculateDirection);
    vBox.getChildren().addAll(titulo, new Label(" "), calculateSurfaceAndVolume, new Label(" "), calculateBase, new Label(" "), calculateModuleAndDirection);
    pane.getChildren().add(vBox);
  }
}
