package algebra.lineal.ui;

import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class SufaceAndVolumeWindows {
  public SufaceAndVolumeWindows() {
    initComponent();
  }

  private void initComponent() {
    Stage selectDifficulties = new Stage();
    Button surface = new Button("Surface");
    surface.setCursor(Cursor.HAND);
    surface.setStyle("-fx-border-color: #00264950; -fx-border-width: 5px; -fx-background-color: #FFFFFF;");
    surface.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.ITALIC, 15));
    Button volume = new Button("Volume");
    volume.setCursor(Cursor.HAND);
    volume.setStyle("-fx-border-color: #00264950; -fx-border-width: 5px; -fx-background-color: #FFFFFF;");
    volume.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.ITALIC, 15));
    //Label label = new Label("Digite un numero");
    //TextField textField = new TextField();
    //surface.setOnAction(event -> {System.out.println(textField.getText());});
    Button exit = new Button("exit");
    exit.setCursor(Cursor.HAND);
    exit.setStyle("-fx-border-color: #00264950; -fx-border-width: 5px; -fx-background-color: #FFFFFF;");
    exit.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.ITALIC, 15));
    surface.setOnAction(event -> {
      SurfaceUI surface1 = new SurfaceUI();});
    volume.setOnAction(event -> {
      VolumeUI volumeUI = new VolumeUI();});
    exit.setOnAction(event -> {selectDifficulties.close();});
    Group root = new Group();
    VBox box1 = new VBox();
    box1.getChildren().addAll(new Label(" "), surface, new Label(" "), volume, new Label(" "), exit);
    root.getChildren().addAll(box1);
    Scene scene = new Scene(root, 1280, 720);
    scene.setFill(Paint.valueOf("#D49E2A"));
    selectDifficulties.setScene(scene);
    //Stage close = (Stage) pane.getScene().getWindow();
    //close.close();
    selectDifficulties.setResizable(false);
    selectDifficulties.show();
  }
}
