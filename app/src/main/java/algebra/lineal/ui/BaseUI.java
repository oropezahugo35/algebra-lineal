package algebra.lineal.ui;

import algebra.lineal.functions.Base;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class BaseUI {

  public BaseUI() {
    initComponent();
  }

  private void initComponent() {
    Stage selectDifficulties = new Stage();
    Label cantidadVecotes = new Label("<Cuantos vectores tendra el plano>");
    TextField cantidadVecotesText = new TextField();
    Label espacioVectorial = new Label("<De que espacio vectorial seran los vectores>:");
    Label opcion = new Label("1.- R2 / 2.- R3 / 3.- R4 / 4.- R5 / 5.- R6");
    TextField espacioVectorialText = new TextField();
    //Base base = new Base(Integer.parseInt(cantidadVecotesText.getText()), Integer.parseInt(espacioVectorialText.getText()));
    Button continueButton = new Button("Continue");
    continueButton.setCursor(Cursor.HAND);
    continueButton.setStyle("-fx-border-color: #00264950; -fx-border-width: 5px; -fx-background-color: #FFFFFF;");
    continueButton.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.ITALIC, 15));
    Button exit = new Button("Exit");
    exit.setCursor(Cursor.HAND);
    exit.setStyle("-fx-border-color: #00264950; -fx-border-width: 5px; -fx-background-color: #FFFFFF;");
    exit.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.ITALIC, 15));
    //Label label = new Label("Digite un numero");
    //TextField textField = new TextField();
    //surface.setOnAction(event -> {System.out.println(textField.getText());});
    continueButton.setOnAction(event -> {
      System.out.println("Hola");});
    exit.setOnAction(event -> {selectDifficulties.close();});
    Group root = new Group();
    VBox box1 = new VBox();
    box1.getChildren().addAll(new Label(" "), cantidadVecotes, new Label(" "), cantidadVecotesText, new Label(" "), espacioVectorial, opcion, new Label(" "), espacioVectorialText, new Label(" "), continueButton, new Label(" "), exit);
    root.getChildren().addAll(box1);
    Scene scene = new Scene(root, 1280, 720);
    scene.setFill(Paint.valueOf("#D49E2A"));
    selectDifficulties.setScene(scene);
    //Stage close = (Stage) pane.getScene().getWindow();
    //close.close();
    selectDifficulties.setResizable(false);
    selectDifficulties.show();
  }
}
