package algebra.lineal.ui;

import algebra.lineal.classes.Vector;
import algebra.lineal.common.ComposableComponent;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class ResultVectorWindow implements ComposableComponent {

  private final int options;
  private final Vector vector;
  private final Pane pane;
  private final VBox vBox;
  private final Label labelResults;
  private final Button exit;

  public ResultVectorWindow(Vector vector, int options) {
    this.options = options;
    this.vector = vector;
    this.pane = new Pane();
    this.vBox = new VBox();
    this.labelResults = new Label(calculate());
    this.exit = new Button("exit");
  }

  @Override
  public void configureComponent() {
  }

  @Override
  public Node getComponent() {
    return pane;
  }

  private String calculate() {
    String result = "";
    switch (options){
      case 0:
        return vector.getName() + " module = " + vector.getVectorModule();
      case 1:
        if (vector.isHasDirection()) {
          if(vector.getDimension() == 3) {
            result += "alfa = " + vector.getVectorDirection()[0] + "\n";
            result += "beta = " + vector.getVectorDirection()[1] + "\n";
            result += "tita = " + vector.getVectorDirection()[2] + "\n";
          } else {
            result += "alfa = " + vector.getVectorDirection()[0] + "\n";
          }
        } else {
          result += "no calculable direction\n";
        }
        return vector.getName() + " direction: " + result;
      default:
        return "something went wrong";
    }
  }

  public void exit(ActionEvent open) {
    Stage stage = (Stage) pane.getScene().getWindow();
    stage.close();
  }
  @Override
  public void compose() {
    exit.setOnAction(this::exit);
    this.vBox.getChildren().addAll(labelResults, exit);
    pane.getChildren().add(vBox);
  }
}
