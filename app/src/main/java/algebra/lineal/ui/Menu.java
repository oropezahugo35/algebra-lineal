package algebra.lineal.ui;

import algebra.lineal.common.ComposableComponent;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import static algebra.lineal.JavaFX.buildPane;

public class Menu implements ComposableComponent {

  private final Pane menu;
  private final Button vectorOperative;
  private final Button matrixOperatives;
  private final VBox vbox;
  private final Button exit;
  private final Label titulo;
  private final Label titulo2;
  private final HBox caja1;
  private final HBox caja2;

  public Menu() {
    /*
    Image image = null;
    try {
      image = new Image(new FileInputStream("java/algebra/lineal/ui/algebraLineal.jpeg"));
    } catch (FileNotFoundException e) {
      System.out.println("Fallo del sistema");
    }
    ImageView imageView = new ImageView(image);
    */
    this.menu = new Pane();
    vectorOperative = new Button("Vector");
    vectorOperative.setCursor(Cursor.HAND);
    vectorOperative.setStyle("-fx-border-color: #00264950; -fx-border-width: 5px; -fx-background-color: #FFFFFF;");
    vectorOperative.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.ITALIC, 15));
    matrixOperatives = new Button("Matrix");
    matrixOperatives.setCursor(Cursor.HAND);
    matrixOperatives.setStyle("-fx-border-color: #00264950; -fx-border-width: 5px; -fx-background-color: #FFFFFF;");
    matrixOperatives.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.ITALIC, 15));
    this.vbox = new VBox();
    this.exit = new Button("Exit");
    exit.setCursor(Cursor.HAND);
    exit.setStyle("-fx-border-color: #00264950; -fx-border-width: 5px; -fx-background-color: #FFFFFF;");
    exit.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.ITALIC, 15));
    titulo = new Label("LINEAR ALGEBRA");
    titulo.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.REGULAR, 45));
    titulo2 = new Label("<Matrix and Vector Operation Calculator>");
    titulo2.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.REGULAR, 38));
    caja1 = new HBox();
    caja1.getChildren().addAll(matrixOperatives);
    caja2 = new HBox();
    caja2.getChildren().addAll(vectorOperative);
  }

  @Override
  public void configureComponent() {

    menu.setStyle("-fx-background-color: A7FBF1");
    menu.setPrefHeight(720);
    menu.setPrefWidth(1280);

    //vectorOperative.setStyle("-fx-background-color: A7FBF1");
    vectorOperative.setPrefHeight(60);
    vectorOperative.setPrefWidth(150);

    //matrixOperatives.setStyle("-fx-background-color: #000000");
    matrixOperatives.setPrefHeight(60);
    matrixOperatives.setPrefWidth(150);

    //exit.setStyle("-fx-background-color: #000000");
    exit.setPrefHeight(60);
    exit.setPrefWidth(150);

    vbox.setLayoutX(500);
    vbox.setLayoutY(157);
    vbox.setSpacing(30);
    vbox.setAlignment(Pos.CENTER);
  }

  @Override
  public Node getComponent() {
    return menu;
  }

  /**
   * exit method close the game.
   */
  public void exit(ActionEvent open) {
    Stage stage = (Stage) menu.getScene().getWindow();
    stage.close();
  }

  public void size(ActionEvent open) {
    Stage selectDifficulties = new Stage();
    Scene scene = new Scene(buildPane(new SizeMenu()),1280,720);
    selectDifficulties.setScene(scene);
    Stage close = (Stage) menu.getScene().getWindow();
    close.close();
    selectDifficulties.setResizable(false);
    selectDifficulties.show();
  }

  public void vectorOperations(ActionEvent open) {
    Stage selectDifficulties = new Stage();
    Scene scene = new Scene(buildPane(new SetVector()),1280,720);
    selectDifficulties.setScene(scene);
    Stage close = (Stage) menu.getScene().getWindow();
    //scene.setFill(Paint.valueOf("#D49E2A"));
    close.close();
    selectDifficulties.setResizable(false);
    selectDifficulties.show();
  }

  @Override
  public void compose() {
    exit.setOnAction(this::exit);
    matrixOperatives.setOnAction(this::size);
    vectorOperative.setOnAction(this::vectorOperations);
    vbox.getChildren().addAll(titulo, titulo2, matrixOperatives, vectorOperative, exit);
    menu.getChildren().add(vbox);
  }
}
