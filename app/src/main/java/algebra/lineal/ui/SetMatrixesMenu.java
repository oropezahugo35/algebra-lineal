package algebra.lineal.ui;

import algebra.lineal.classes.Matrix;
import algebra.lineal.common.ComposableComponent;
import algebra.lineal.functions.MatrixOperator;
import algebra.lineal.functions.StringManager;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import static algebra.lineal.JavaFX.buildPane;

public class SetMatrixesMenu implements ComposableComponent {

  private final Pane pane;
  private final int rowNum1;
  private final int columnNum1;
  private final int rowNum2;
  private final int columnNum2;
  private final String name1;
  private final String name2;
  private final VBox vBox;
  private final VBox vBox2;
  private final SetMatrixRows[] setMatrixeslist;
  private final SetMatrixRows setMatrixes1 = new SetMatrixRows(1);
  private final SetMatrixRows setMatrixes2 = new SetMatrixRows(2);
  private final SetMatrixRows setMatrixes3 = new SetMatrixRows(3);
  private final SetMatrixRows setMatrixes4 = new SetMatrixRows(4);
  private final SetMatrixRows setMatrixes5 = new SetMatrixRows(5);
  private MatrixOperator matrixOperator;
  private final Button product;
  private final Button addition;

  public SetMatrixesMenu(Matrix matrix1, Matrix matrix2) {
    this.pane = new Pane();
    this.vBox = new VBox();
    this.vBox2 = new VBox();
    this.rowNum1 = matrix1.getDimension1();
    this.columnNum1 = matrix1.getDimension2();
    this.name1 = matrix1.getName();
    this.name2 = matrix2.getName();
    this.rowNum2 = matrix2.getDimension1();
    this.columnNum2 = matrix2.getDimension2();
    this.product = new Button("product");
    this.addition = new Button("addition");
    setMatrixeslist = new SetMatrixRows[]{setMatrixes1,setMatrixes2, setMatrixes3, setMatrixes4, setMatrixes5};

  }

  @Override
  public void configureComponent() {
    pane.setPrefHeight(720);
    pane.setPrefWidth(1280);
    pane.setStyle("-fx-background-color: A7FBF1");

    vBox.setAlignment(Pos.CENTER);
  }

  @Override
  public Node getComponent() {
    return this.pane;
  }

  private void product(Button button) {
    button.setOnAction(event -> {
      Stage selectDifficulties = new Stage();
      Matrix matrix1 = new Matrix(rowNum1, columnNum1, name1);
      Matrix matrix2 = new Matrix(rowNum2, columnNum2, name2);

      for(int i = 0; i < rowNum1; i++) {
        matrix1.setMatrixRow(i, new StringManager(setMatrixeslist[i].getTextField1().getText(), columnNum1).convert());
      }
      for(int i = 0; i < rowNum2; i++) {
        matrix2.setMatrixRow(i, new StringManager(setMatrixeslist[i].getTextField2().getText(), columnNum2).convert());
      }

      matrixOperator = new MatrixOperator(matrix1, matrix2, matrix1.getName() + "x" + matrix2.getName());

      Scene scene = new Scene(buildPane(new ResultWindow(this.matrixOperator, true)),1280,720);
      selectDifficulties.setScene(scene);
      Stage close = (Stage) pane.getScene().getWindow();
      close.close();
      selectDifficulties.setResizable(false);
      selectDifficulties.show();
    });
  }

  private void addition(Button button) {
    button.setOnAction(event -> {
      Stage selectDifficulties = new Stage();
      Matrix matrix1 = new Matrix(rowNum1, columnNum1, name1);
      Matrix matrix2 = new Matrix(rowNum2, columnNum2, name2);

      for(int i = 0; i < rowNum1; i++) {
        matrix1.setMatrixRow(i, new StringManager(setMatrixeslist[i].getTextField1().getText(), columnNum1).convert());
      }
      for(int i = 0; i < rowNum2; i++) {
        matrix2.setMatrixRow(i, new StringManager(setMatrixeslist[i].getTextField2().getText(), columnNum2).convert());
      }
      matrixOperator = new MatrixOperator(matrix1, matrix2, name1 + "+" + name2);
      Scene scene = new Scene(buildPane(new ResultWindow(this.matrixOperator, false)),1280,720);
      selectDifficulties.setScene(scene);
      Stage close = (Stage) pane.getScene().getWindow();
      close.close();
      selectDifficulties.setResizable(false);
      selectDifficulties.show();
    });
  }

  private int whichIsMajor() {
    return (rowNum2 < rowNum1) ? rowNum1: rowNum2;
  }
  @Override
  public void compose() {

    product(this.product);
    addition(this.addition);
    Label label = new Label("                             " + name1 +
            "                                            " + name2);

    if(whichIsMajor() == 1) {
      vBox.getChildren().addAll(label, setMatrixeslist[0].build(), product, addition);
    } else if (whichIsMajor() == 2) {
      vBox.getChildren().addAll(label, setMatrixeslist[0].build(),
              setMatrixeslist[1].build(), product, addition);
    } else if (whichIsMajor() == 3) {
      vBox.getChildren().addAll(label, setMatrixeslist[0].build(),
              setMatrixeslist[1].build(),
              setMatrixeslist[2].build(), product, addition);
    } else if (whichIsMajor() == 4) {
      vBox.getChildren().addAll(label, setMatrixeslist[0].build(),
              setMatrixeslist[1].build(),
              setMatrixeslist[2].build(),
              setMatrixeslist[3].build(), product, addition);
    } else if (whichIsMajor() == 5) {
      vBox.getChildren().addAll(label, setMatrixeslist[0].build(),
              setMatrixeslist[1].build(),
              setMatrixeslist[2].build(),
              setMatrixeslist[3].build(),
              setMatrixeslist[4].build(), product, addition);
    } else {
      vBox.getChildren().add(new Label("something went wrong"));
    }

    pane.getChildren().addAll(vBox);
  }
}
