package algebra.lineal.ui;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;

public class Ejemplo {

  public Ejemplo() {
    initComponent();
  }

  private void initComponent() {
    Stage selectDifficulties = new Stage();
    Button surface = new Button("Surface");
    Label label = new Label("Digite un numero");
    TextField textField = new TextField();
    surface.setOnAction(event -> {System.out.println(textField.getText());});
    Button exit = new Button("exit");
    exit.setOnAction(event -> {selectDifficulties.close();});
    Group root = new Group();
    VBox box1 = new VBox();
    box1.getChildren().addAll(label, textField, surface, exit);
    root.getChildren().addAll(box1);
    Scene scene = new Scene(root, 1280, 720);
    scene.setFill(Paint.valueOf("#D49E2A"));
    selectDifficulties.setScene(scene);
    //Stage close = (Stage) pane.getScene().getWindow();
    //close.close();
    selectDifficulties.setResizable(false);
    selectDifficulties.show();
  }
}
