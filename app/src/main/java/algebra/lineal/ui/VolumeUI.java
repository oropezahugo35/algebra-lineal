package algebra.lineal.ui;

import algebra.lineal.functions.Volume;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import javax.swing.*;

public class VolumeUI {
  Volume volume = new Volume();
  Label Resultado = new Label("");
  public VolumeUI() {
    initComponent();
  }

  private void initComponent() {
    Stage selectDifficulties = new Stage();
    Label titulo = new Label("SURFACE");
    Label option = new Label("<Choose a Option>:");
    VBox caja = new VBox();
    Label option1 = new Label("Digite 1 para hallar la superficie de un Paralelogramo");
    Label option2 = new Label("Digite 2 para hallar la superficie de un Triangulo");
    TextField optionResult = new TextField();
    caja.getChildren().addAll(new Label(" "), option1, new Label(" "), option2, new Label(" "), optionResult);
    Label primerV = new Label("First Vector:");
    HBox caja1 = new HBox();
    Label u1Text = new Label("u1:  ");
    TextField u1 = new TextField();
    Label u2Text = new Label("u2:  ");
    TextField u2 = new TextField();
    Label u3Text = new Label("u3:  ");
    TextField u3 = new TextField();
    caja1.getChildren().addAll(new Label(" "), u1Text, u1, new Label("   "), u2Text, u2, new Label("   "), u3Text, u3);
    Label segundoV = new Label("Second Vector:");
    HBox caja2 = new HBox();
    Label v1Text = new Label("v1:  ");
    TextField v1 = new TextField();
    Label v2Text = new Label("v2:  ");
    TextField v2 = new TextField();
    Label v3Text = new Label("v3:  ");
    TextField v3 = new TextField();
    caja2.getChildren().addAll(new Label(" "), v1Text, v1, new Label("   "), v2Text, v2, new Label("   "), v3Text, v3);
    Label tercerV = new Label("Third Vector:");
    HBox caja3 = new HBox();
    Label w1Text = new Label("w1:  ");
    TextField w1 = new TextField();
    Label w2Text = new Label("w2:  ");
    TextField w2 = new TextField();
    Label w3Text = new Label("w3:  ");
    TextField w3 = new TextField();
    caja3.getChildren().addAll(new Label(" "), w1Text, w1, new Label("   "), w2Text, w2, new Label("   "), w3Text, w3);
    Button calcular = new Button("Calculate");
    calcular.setCursor(Cursor.HAND);
    calcular.setStyle("-fx-border-color: #00264950; -fx-border-width: 5px; -fx-background-color: #FFFFFF;");
    calcular.setFont(Font.font("Montserrat", FontWeight.BOLD, FontPosture.ITALIC, 15));
    //Label label = new Label("Digite un numero");
    //TextField textField = new TextField();
    //surface.setOnAction(event -> {System.out.println(textField.getText());});
    calcular.setOnAction(event -> {
      int number = Integer.parseInt(optionResult.getText());
      double u1valor = Double.parseDouble(u1.getText());
      double u2valor = Double.parseDouble(u2.getText());
      double u3valor = Double.parseDouble(u3.getText());
      double v1valor = Double.parseDouble(v1.getText());
      double v2valor = Double.parseDouble(v2.getText());
      double v3valor = Double.parseDouble(v3.getText());
      double w1valor = Double.parseDouble(w1.getText());
      double w2valor = Double.parseDouble(w2.getText());
      double w3valor = Double.parseDouble(w3.getText());
      String resultado = volume.menuCalculationVolumes(number, u1valor, u2valor, u3valor, v1valor, v2valor, v3valor, w1valor, w2valor, w3valor);
      JOptionPane.showMessageDialog(null, resultado, "RESULT", JOptionPane.ERROR_MESSAGE);
    });
    Group root = new Group();
    VBox box1 = new VBox();
    box1.getChildren().addAll(new Label(" "), titulo, option, caja, new Label(" "), primerV, caja1, new Label(" "), segundoV, caja2, new Label(" "), tercerV, caja3, new Label(" "),calcular);
    root.getChildren().addAll(box1);
    Scene scene = new Scene(root, 1280, 720);
    scene.setFill(Paint.valueOf("#D49E2A"));
    selectDifficulties.setScene(scene);
    selectDifficulties.setResizable(false);
    selectDifficulties.show();
  }
}
