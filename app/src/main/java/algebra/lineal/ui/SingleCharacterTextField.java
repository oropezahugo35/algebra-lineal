package algebra.lineal.ui;

import javafx.scene.control.TextField;

public class SingleCharacterTextField extends TextField {

  /**
   * SingleCharacterTextField constructor initialize the class with an empty text.
   */
  public SingleCharacterTextField() {
    super("");
  }

  /**
   * SingleCharacterTextField constructor initialize the class with the requested text.
   */
  public SingleCharacterTextField(String text) {
    super(text);
  }

  @Override
  public void replaceText(int start, int end, String text) {
    super.replaceText(start, end, text);
    verifySingleCharacter();
  }

  @Override
  public void replaceSelection(String text) {
    super.replaceSelection(text);
    verifySingleCharacter();
  }

  /**
   * verifySingleCharacter checks if the entered text is one single character,
   * and overwrites it if is longer.
   */
  protected char verifySingleCharacter() {
    if (this.getText().length() > 0  && this.getText() != null) {
      this.setText(String.valueOf(this.getText().charAt(this.getText().length() - 1)));
      return this.getText().charAt(this.getText().length() - 1);
    }
    return '0';
  }

}
