package algebra.lineal.ui;

import algebra.lineal.common.Component;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

public class SetMatrixRows implements Component {

  private HBox hBox;
  private TextField textField1;
  private TextField textField2;
  private int rowNum;

  public SetMatrixRows(int rowNum) {
    this.rowNum = rowNum;
    this.hBox = new HBox();
    this.textField1 = new TextField();
    this.textField2 = new TextField();
  }

  public TextField getTextField1() {
    return textField1;
  }

  public TextField getTextField2() {
    return textField2;
  }

  @Override
  public void configureComponent() {
    hBox.getChildren().addAll(new Label("row " + rowNum), textField1, new Label("row " + rowNum), textField2);
  }

  @Override
  public Node getComponent() {
    return hBox;
  }
}
