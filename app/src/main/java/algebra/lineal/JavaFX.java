package algebra.lineal;


import algebra.lineal.common.Component;
import algebra.lineal.ui.Menu;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class JavaFX extends Application {

  private Stage mainMenu;
  private Scene scene;

  @Override
  public void start(Stage stage) {
    this.mainMenu = stage;

    Menu menu = new Menu();
    menu.build();

    Scene scene = new Scene((Pane) menu.getComponent(), 640, 480);
    this.scene = scene;
    stage.setTitle("TRABAJO PRACTICO DE ALGEBRA LINEAL");
    //stage.setStyle("-fx-background-color:#95F4F4");
    //stage.setValue
    stage.setScene(scene);
    stage.show();
  }

  public static void main(String[] args) {
    launch();
  }

  public static Pane buildPane(Component component) {
    var pane = new Pane();
    pane.getChildren().addAll(component.build());
    pane.setStyle("white");
    return pane;
  }
}
